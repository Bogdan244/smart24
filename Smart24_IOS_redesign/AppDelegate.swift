//
//  AppDelegate.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/14/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import UserNotifications
import FacebookCore
import SwiftyVK
import GoogleSignIn
import iCloudDocumentSync
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var providerDelegate: CallKitProviderDelegate?
    var account: VSLAccount!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        Fabric.with([Crashlytics.self])
        
        // checking first run
        if UserDefaults.standard.bool(forKey: Constants.firstRun) {
            if let viewController = R.storyboard.main.loginViewController() {
                let navigationController = Smart24NavigationController(rootViewController: viewController)
                window = UIWindow(frame: UIScreen.main.bounds)
                window?.rootViewController = navigationController
                window?.makeKeyAndVisible()
            }
        } else {
            if let viewController = R.storyboard.main.startPageViewController() {
                let navigationController = Smart24NavigationController(rootViewController: viewController)
                window = UIWindow(frame: UIScreen.main.bounds)
                window?.rootViewController = navigationController
                window?.makeKeyAndVisible()
            }
        }
        
        // icloud init
        iCloud.shared().setupiCloudDocumentSync(withUbiquityContainer: nil)
        
        // iOS 10 support
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        // iOS 9 support
        else if #available(iOS 9, *) {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        // init SIP
        setupVialerEndpoint()
        setupSIPAccount()
        
        return true
    }
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any]) -> Bool {
        if SDKApplicationDelegate.shared.application(application, open: url, options: options) {
            return true
        }
        
        if GIDSignIn.sharedInstance().handle(url,
                                             sourceApplication: options[ .sourceApplication] as? String,
                                             annotation: options[ .annotation]) {
            return true
        }
        
        /*
         if OKSDK.open(url) {
         return true
         } */
        
        let app = options[.sourceApplication] as? String
        VK.process(url: url, sourceApplication: app)
        return true
    }
    
    func application(_ application: UIApplication, handleActionWithIdentifier identifier: String?, for notification: UILocalNotification, completionHandler: @escaping () -> Swift.Void) {
        // Must be called when finished
        completionHandler()
    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification){
        
        if application.applicationState == .inactive {
        }
    }
    
    private func setupVialerEndpoint() {
        DDLogWrapper.setup()
        
        let endpointConfiguration = VSLEndpointConfiguration()
        endpointConfiguration.logLevel = 3
        endpointConfiguration.userAgent = "VialerSIPLib Example App"
        if let transportType = VSLTransportConfiguration(transportType: .UDP) {
            endpointConfiguration.transportConfigurations = [transportType]
        }
        
        do {
            try VialerSIPLib.sharedInstance().configureLibrary(withEndPointConfiguration: endpointConfiguration)
        } catch let error {
            debugPrint("Error setting up VialerSIPLib: \(error)")
        }
    }
    
    private func setupSIPAccount() {
        do {
            account = try VialerSIPLib.sharedInstance().createAccount(withSip: SipUser())
        } catch let error {
            debugPrint("Could not create account. Error:\(error)\nExiting")
            assert(false)
        }
    }
}

