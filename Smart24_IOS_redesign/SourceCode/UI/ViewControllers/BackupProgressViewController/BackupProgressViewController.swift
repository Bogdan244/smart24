//
//  BackupProgressViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/14/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift
import iCloudDocumentSync

class BackupProgressViewController: UIViewController {
    
    @IBOutlet weak var rateProgressLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    
    var backupStatus = BackupState.createBackup
    
    // MARK private properties
    private let outerCircle            = CAShapeLayer()
    private let decreaseCircleProgress = CAShapeLayer()
    private let timerStep: CGFloat     = 0.1
    
    private var timer                  = Timer()
    private var seconds                = CGFloat()
    private var percent                = Int()
    
    private let viewModel              = BackupViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        configureSignals()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if isTimerRunning() {
            stop(showMessage: true)
        }
    }
    
    private func configureView() {
        backgroundImageView.roundedView(border: false, borderColor: nil, borderWidth: nil, cornerRadius: nil)
        
        setTimerProgressToAvatar()
        rateProgressLabel.textColor = ColorName.BlueProgress.color
        
        if backupStatus == .createBackup {
            statusLabel.text = "Сохранение контактов..."
        } else {
            statusLabel.text = "Восстановление контактов..."
        }
    }
    
    private func configureSignals() {
        start()
        
        viewModel.fetchAddressBookContacts(fetchType: backupStatus)
            .observe(on: UIScheduler())
            .startWithSignal { [weak self] signal, _ in
                
                signal.observeCompleted {
                    debugPrint("observeCompleted")
                }
                
                signal.observeFailed { error in
                    debugPrint("observeFailed")
                }
                
                signal.observeInterrupted {
                    self?.showBackupError()
                }
        }
        
        if backupStatus == .getBackup {
            viewModel.fileListQuery(pathToFile: iCloud.shared().ubiquitousDocumentsDirectoryURL())
        }
    }
    
    private func showBackupError() {
        stop(showMessage: false)
        let alert = UIAlertController(title: nil, message: Constants.backupErrorMessage, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .cancel) { [weak self] action in
            if action.style == .cancel {
                self?.viewModel.backToPreviousVC()
            }
        })
        
        alert.addAction(UIAlertAction(title: "Разрешить доступ", style: .default) { [weak self] action in
            self?.viewModel.backToPreviousVC()
            self?.openSystemSettings()
        })
        
        present(alert, animated: true, completion: nil)
    }
    
    private func openSystemSettings() {
        if let url = URL(string: UIApplicationOpenSettingsURLString) {
            UIApplication.shared.openURL(url)
        }
    }
    
    private func setTimerProgressToAvatar() {
        let frameSize = backgroundImageView.frame.size
        
        decreaseCircleProgress.path = UIBezierPath(ovalIn: CGRect(x: 8, y: 8, width: frameSize.width - 16, height: frameSize.height - 16)).cgPath
        decreaseCircleProgress.lineWidth = 8
        decreaseCircleProgress.strokeEnd = 1
        decreaseCircleProgress.lineCap = kCALineCapRound
        decreaseCircleProgress.fillColor = UIColor.clear.cgColor
        decreaseCircleProgress.strokeColor = ColorName.LightBlueProgress.color.cgColor
        backgroundImageView.layer.addSublayer(decreaseCircleProgress)
        
        outerCircle.path = UIBezierPath(ovalIn: CGRect(x: 8, y: 8, width: frameSize.width - 16, height: frameSize.height - 16)).cgPath
        outerCircle.lineWidth = 8
        outerCircle.lineCap = kCALineCapRound
        outerCircle.fillColor = UIColor.clear.cgColor
        outerCircle.strokeColor = ColorName.BlueProgress.color.cgColor
        outerCircle.strokeEnd = seconds
        backgroundImageView.layer.addSublayer(outerCircle)
    }
    
    private func closeCircleView() {
        stop(showMessage: true)
        
        view.removeFromSuperview()
        removeFromParentViewController()
        
        if let presentingVC = presentingViewController {
            presentingVC.dismiss(animated: true, completion: nil);
        }
    }
    
    private func start() {
        seconds = 0
        timer = Timer.scheduledTimer(timeInterval: Double(timerStep), target: self, selector:  #selector(update), userInfo: nil, repeats: true)
    }
    
    private func stop(showMessage: Bool) {
        timer.invalidate()
        
        if !showMessage {
            return
        }
        
        let alert = UIAlertController(title: "", message: Constants.savedMessage, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .cancel) { [weak self] _  in
            self?.viewModel.backToPreviousVC()
        }
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    private func showSuccessMessage() {
        
    }
    
    private func isTimerRunning() -> Bool {
        return timer.isValid
    }
    
    func update() {
        if (outerCircle.strokeEnd > 1) {
            stop(showMessage: true)
        } else {
            seconds = seconds + timerStep
            outerCircle.strokeEnd = seconds * 0.1
            percent = Int(seconds * 10)
            rateProgressLabel.text = "\(percent)%"
        }
    }
}

