//
//  StartViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/14/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class StartPageViewController: UIViewController, UIPageViewControllerDelegate {
    
    // The custom UIPageControl
    @IBOutlet weak var pageControl: UIPageControl!
    
    var previousIndex: Int?
    var pendingIndex = 1
    
    private var pageContainer: UIPageViewController!
    private var currentIndex = 0
    
    private let firstPage   = R.storyboard.main.firstStartPageViewController()
    private let secondPage  = R.storyboard.main.secondStartPageViewController()
    private let thirdPage   = R.storyboard.main.thirdStartPageViewController()
    
    var pagesArray          = [UIViewController]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pagesArray.append(firstPage!)
        pagesArray.append(secondPage!)
        pagesArray.append(thirdPage!)
        
        firstPage?.pageContainer = self
        secondPage?.pageContainer = self
        thirdPage?.pageContainer = self
        
        pageContainer = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageContainer.delegate = self
        pageContainer.dataSource = self
        pageContainer.setViewControllers([pagesArray.first!], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        view.addSubview(pageContainer.view)
        
        view.bringSubview(toFront: pageControl)
        pageControl.numberOfPages = pagesArray.count
        pageControl.currentPage = 0
        pendingIndex = 1
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func openNextPage() {
        pageContainer.setViewControllers([pagesArray[pendingIndex]], direction: .forward, animated: true, completion: nil )
        pageControl.currentPage = pendingIndex
        currentIndex = pendingIndex
        pendingIndex = pendingIndex + 1
        
        previousIndex = currentIndex == 0 ? nil : currentIndex - 1
    }
    
    func openPreviousPage() {
        if let index = previousIndex {
            pageContainer.setViewControllers([pagesArray[index]], direction: .reverse, animated: true, completion: nil )
            pageControl.currentPage = index
            currentIndex = index
            pendingIndex = index + 1
            
            previousIndex = currentIndex == 0 ? nil : currentIndex - 1
        }
    }
    
    // MARK: - UIPageViewController delegates
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if let index = pagesArray.index(of: pendingViewControllers.first!) {
            pendingIndex = index
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            currentIndex = pendingIndex
            pageControl.currentPage = currentIndex
        }
    }
}

// MARK: UIPageViewControllerDataSource
extension StartPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex = pagesArray.index(of: viewController)!
        if currentIndex == 0 {
            return nil
        }
        previousIndex = abs((currentIndex - 1) % pagesArray.count)
        
        return pagesArray[previousIndex!]
    }
    
    func pageViewController(_ viewControllerBeforepageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = pagesArray.index(of: viewController)!
        if currentIndex == pagesArray.count - 1 {
            return nil
        }
        pendingIndex = abs((currentIndex + 1) % pagesArray.count)
        previousIndex = abs((currentIndex) % pagesArray.count)
        
        return pagesArray[pendingIndex]
    }
}
