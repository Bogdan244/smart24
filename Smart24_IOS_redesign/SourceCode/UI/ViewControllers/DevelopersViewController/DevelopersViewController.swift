//
//  DevelopersViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/10/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class DevelopersViewController: UIViewController {

    @IBAction func openUkraineSmartPageAction(_ sender: Any) {
        if let url = URL(string: Constants.smartlLink) {
            UIApplication.shared.openURL(url)
        }
    }
    @IBAction func openRussianSmartPageAction(_ sender: Any) {
        if let url = URL(string: Constants.smartlLink) {
            UIApplication.shared.openURL(url)
        }
    }
    @IBAction func openFreshDigitalPageAction(_ sender: Any) {
        if let url = URL(string: Constants.freshDigitalLink) {
            UIApplication.shared.openURL(url)
        }
    }
    @IBAction func openWezomPageAction(_ sender: Any) {
        if let url = URL(string: Constants.wezomLink) {
            UIApplication.shared.openURL(url)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let shieldButton = UIBarButtonItem(image: R.image.white_shield_icon(), style: .plain , target: self, action: #selector(shieldButtonTapped))
        navigationItem.rightBarButtonItem = shieldButton
    }
    
    func shieldButtonTapped() {
        showAlertMessage(title: nil, message: Constants.shialdMessage)
    }
}
