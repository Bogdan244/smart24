//
//  ProfileViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/27/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa
import MBProgressHUD

class ProfileViewController: UITableViewController {
    
    //MARK:- private properties
    private var user: UserEntity?
    
    private let headerView          = ProfileHeaderView.instanciateFromNib()
    private let profileCellHeader   = ProfileCellHeader.instanciateFromNib()
    
    private let profileViewModel    = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        configureNavigationViewController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureSignals()
        updateHeaderView()
    }
    
    private func configureTableView() {
        user = profileViewModel.getSelfEntity()
        
        tableView.register(ProfileOptionCell.self)
        tableView.register(ProfileNotificationCell.self)
        tableView.tableHeaderView = headerView
        
        profileCellHeader.activateButton.setClearBackgrounStyle()
        profileCellHeader.activateButton.addTarget(self, action: #selector(showActivateServiewAlert), for: .touchUpInside)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    private func updateHeaderView() {
        headerView.updateViewWithModel(model: user)
        headerView.circleAvatarView.changePhotoButton.addTarget(self, action: #selector(openSettings), for: .touchUpInside)
        headerView.setMessagesCount(count: profileViewModel.unreadMessagesCount())
    }
    
    private func configureNavigationViewController() {
        
        let settings = UIBarButtonItem(image: R.image.white_settings_icon(), style: .plain , target: self, action: #selector(openSettings))
        let shield = UIBarButtonItem(image: R.image.white_shield_icon(), style: .plain , target: self, action: #selector(shieldButtonTapped))
        
        navigationItem.rightBarButtonItems = [shield, settings]
    }
    
    func openSettings() {
        profileViewModel.openChangeDataVC()
        revealViewController().panGestureRecognizer().isEnabled = false
    }
    
    func shieldButtonTapped() {
        showAlertMessage(title: nil, message: Constants.shialdMessage)
    }
    
    private func configureSignals() {
        guard let userId = user?.uid else {
            return
        }
        
        profileViewModel.getActiveServices(userId: userId)
            .observe(on: UIScheduler())
            .start { [weak self] events in
                if events.isCompleted {
                    self?.tableView.reloadData()
                }
                
                if let services = self?.profileViewModel.activeServices.value, services.count != 0 {
                    self?.headerView.activeSarvicesLabel.text = Constants.servicesExist
                } else {
                    self?.headerView.activeSarvicesLabel.text = Constants.servicesNotExist
                }
        }
        
        if let userId = user?.uid {
            profileViewModel.getNotifications(userId: userId)
                .observe(on: UIScheduler())
                .start { [weak self] events in
                    
                    if events.isCompleted {
                        self?.tableView.reloadData()
                    }
                    
                    if let notificationsCount = self?.profileViewModel.unreadMessagesCount(), notificationsCount != 0 {
                        self?.profileCellHeader.notificationsLabel.text = Constants.notificationsExist
                    } else {
                        self?.profileCellHeader.notificationsLabel.text = Constants.notificationsNotExist
                    }
            }
        }
    }
    
    private func activateServices(userId: String, activateServices: String) {
        MBProgressHUD.showAdded(to: view, animated: true)
        
        profileViewModel.activateService(userId: userId, activationCode: activateServices)
            .observe(on: UIScheduler())
            .start { [weak self] events in
                guard let strongSelf = self else { return }
                
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                if let error = events.error {
                    strongSelf.showSmart24Error(error: error)
                }
                
        }
    }
    
    private func deleteNotification(notification: NotificationEntity) {
        guard let notificationId = Int(notification.uid) else {
            return
        }
        
        MBProgressHUD.showAdded(to: view, animated: true)
        profileViewModel.deleteNotification(userId: profileViewModel.currentId, notificationId: notificationId, action: ApiManagerKey.delete)
            .observe(on: UIScheduler())
            .start { [weak self] events in
                guard let strongSelf = self else { return }
                
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                switch events {
                case .completed:
                    strongSelf.headerView.setMessagesCount(count: strongSelf.profileViewModel.unreadMessagesCount())
                    strongSelf.tableView.reloadData()
                case .failed(_):
                    strongSelf.showAlertMessage(title: nil, message: Constants.notificationError)
                case .interrupted:
                    strongSelf.showAlertMessage(title: nil, message: Constants.notificationError)
                case .value(_):
                    break
                }
        }
        
    }
    
    func showActivateServiewAlert() {
        let alert = UIAlertController(title: "Введите код активации:", message: "", preferredStyle: .alert)
        
        alert.addTextField { textField in
            textField.placeholder = "Код активации:"
        }
        
        alert.addAction(UIAlertAction(title: "ОТМЕНА", style: .cancel, handler: nil))
        
        alert.addAction(UIAlertAction(title: "OK", style: .default) { [weak self] _ in
            if let activationCode = alert.textFields?[0].text, let userId = self?.user?.uid {
                self?.activateServices(userId: userId, activateServices: activationCode)
            }
        })
        
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return profileViewModel.activeServices.value?.count ?? 0
        }
        
        return profileViewModel.notifications.value?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell: ProfileOptionCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.selectionStyle = .none
            if let notification = profileViewModel.activeServices.value?[indexPath.row] {
                cell.updateViewWithModel(model: notification)
            }
            return cell
            
        } else {
            let cell: ProfileNotificationCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            if let notification = profileViewModel.notifications.value?[indexPath.row] {
                cell.accessoryType = .disclosureIndicator
                cell.updateViewWithModel(model: notification)
            }
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return section == 1 ? profileCellHeader : nil
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 1 ? 140 : 0
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCellEditingStyle.delete,
            let message = profileViewModel.notifications.value?[indexPath.row] {
            deleteNotification(notification: message)
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "УДАЛИТЬ"
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section != 0,
            let message = profileViewModel.notifications.value?[indexPath.row] {
            profileViewModel.openNotificationVC(message: message)
        }
    }
}
