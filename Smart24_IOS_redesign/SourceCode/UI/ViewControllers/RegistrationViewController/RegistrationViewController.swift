//
//  RegistrationViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/16/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift
import MBProgressHUD

class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: LoginTextField!
    @IBOutlet weak var surnameTextField: LoginTextField!
    @IBOutlet weak var phoneTextField: LoginTextField!
    @IBOutlet weak var passwordTextField: LoginTextField!
    @IBOutlet weak var confirmPasswordTextField: LoginTextField!
    
    @IBOutlet weak var registrationButton: LoginButton!
    
    @IBAction func howItWorkButtonAction(_ sender: Any) {
        registrationViewModel.openHowItWorkTBC()
    }
    
    private let registrationViewModel = RegistrationViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
        confgiureSignals()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureNavigationController()
    }
    
    private func configureViews() {
        hideKeyboardWhenTappedAround()
        title = "Регистрация"
        phoneTextField.keyboardType = .numberPad
        phoneTextField.delegate = self
    }
    
    private func configureNavigationController() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    private func confgiureSignals() {
        registrationButton.addTarget(registrationViewModel.cocoaActionRegistration, action: CocoaAction<Any>.selector, for: .touchUpInside)
        
        // MARK button enable when email is valid
        registrationButton.reactive.isEnabled <~ registrationViewModel.inputValidData
        
        let nameProducer            = registrationViewModel.racTextProducer(textField: nameTextField)
        let surnameProducer         = registrationViewModel.racTextProducer(textField: surnameTextField)
        let phoneProducer           = registrationViewModel.racTextProducer(textField: phoneTextField)
        let passwordProducer        = registrationViewModel.racTextProducer(textField: passwordTextField)
        let confirmPasswordProduser = registrationViewModel.racTextProducer(textField: confirmPasswordTextField)
        
        registrationViewModel.nameProperty              <~ nameProducer
        registrationViewModel.surnameProperty           <~ surnameProducer
        registrationViewModel.phoneProperty             <~ phoneProducer
        registrationViewModel.passwordProperty          <~ passwordProducer
        registrationViewModel.confirmPasswordProperty   <~ confirmPasswordProduser
        
        registrationViewModel.registrationSignalProducer.events.observe { [weak self] value in
            guard let strongSelf = self else { return }
            
            strongSelf.view.endEditing(true)
            MBProgressHUD.showAdded(to: strongSelf.view, animated: true)
        }
        
        registrationViewModel.registrationSignalProducer
            .events
            .observe(on: UIScheduler())
            .observeValues{ [weak self] (event) in
                guard let strongSelf = self else { return }
                
                switch event {
                case .completed:
                    strongSelf.showConfirmRegistration()
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                    
                case .failed(let error):
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                    strongSelf.showSmart24Error(error: error)
                    
                default:
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                }
        }
    }
    
    private func showConfirmRegistration() {

        let smsAlert = UIAlertController(title: nil, message: "Регистрация прошла успешно", preferredStyle: .alert)
        smsAlert.addAction(UIAlertAction(title: "OK", style: .default) { [weak self] action in
            self?.confirmRegistration()
        })
        
        present(smsAlert, animated: true, completion: nil)
    }
    
    private func confirmRegistration() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        registrationViewModel.loginUser()
            .observe(on: UIScheduler())
            .start { [weak self] event in
                guard let strongSelf = self else { return }
                
                switch event {
                case .failed(let error):
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                    strongSelf.showSmart24Error(error: error)
                    
                case .interrupted:
                    strongSelf.showAlertMessage(title: "Ошибка", message: "Не удалось получить данные с сервера")
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                    
                default:
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                }
        }
    }
}

//MARK:- UITextFieldDelegate delegate
extension RegistrationViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= Constants.phoneCharactersCount
    }
}
