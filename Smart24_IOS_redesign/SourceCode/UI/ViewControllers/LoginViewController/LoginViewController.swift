//
//  LoginViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/15/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa
import MBProgressHUD
import SwiftyVK
import GoogleSignIn

class LoginViewController: UIViewController {
    
    @IBOutlet weak var phoneTextField: LoginTextField!
    @IBOutlet weak var passwordTextField: LoginTextField!
    
    @IBOutlet weak var enterButton: LoginButton!
    
    @IBAction func forgotPasswordButtonAction(_ sender: Any) {
        loginViewModel.openForgotPasswordVC()
    }
    @IBAction func signUpButtonAction(_ sender: Any) {
        loginViewModel.openRegistrationVC()
    }
    @IBAction func vkButtonAction(_ sender: Any) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        VK.logOut()
        VK.configure(withAppId: Constants.vkId, delegate: self)
        
        VK.logIn()
    }
    @IBAction func faceBookButtonAction(_ sender: Any) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        loginViewModel.loginWithFacebook(viewController: self)
            .observe(on: UIScheduler())
            .startWithSignal { [weak self] signal, disposable in
                guard let strongSelf = self else { return }
                
                signal.observeCompleted {
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                }
                
                signal.observeFailed { error in
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                }
        }
    }
    @IBAction func googlePlusButtonAction(_ sender: Any) {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        GIDSignIn.sharedInstance().clientID = Constants.gClientID
        GIDSignIn.sharedInstance().signIn()
    }
    @IBAction func okButtonAction(_ sender: Any) {
        /*
         MBProgressHUD.showAdded(to: self.view, animated: true)
         let settings = OKSDKInitSettings()
         settings.appKey = Constants.okAppKey
         settings.appId = Constants.okAppId
         OKSDK.initWith(settings) */
    }
    
    let loginViewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
        confgiureSignals()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    private func configureViews() {
        hideKeyboardWhenTappedAround()
        phoneTextField.delegate = self
        phoneTextField.keyboardType = .numberPad
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        
        phoneTextField.setLeftImage(image: R.image.gray_phone_icon())
        passwordTextField.setLeftImage(image: R.image.gray_lock_icon())
        
        #if DEBUG
            DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
                self?.phoneTextField.text      = "380990000001"
                self?.passwordTextField.text   = "0000001"
                
                self?.loginViewModel.userPhoneProperty.value = "380990000001"
                self?.loginViewModel.userPasswordProperty.value = "0000001"
            }
        #endif
    }
    
    private func confgiureSignals() {
        
        let userNameProducer        = loginViewModel.racTextProducer(textField: phoneTextField)
        let userPasswordProducer    = loginViewModel.racTextProducer(textField: passwordTextField)
        
        loginViewModel.userPhoneProperty    <~ userNameProducer
        loginViewModel.userPasswordProperty <~ userPasswordProducer
        enterButton.reactive.isEnabled      <~ loginViewModel.loginInputValid
        
        enterButton.addTarget(loginViewModel.cocoaActionLogin, action: CocoaAction<Any>.selector, for: .touchUpInside)
        
        enterButton.reactive
            .controlEvents(.touchUpInside)
            .observe { [weak self] _ in
                guard let strongSelf = self else { return }
                
                strongSelf.view.endEditing(true)
                MBProgressHUD.showAdded(to: strongSelf.view, animated: true)
        }
        
        loginViewModel.loginSignalProducer
            .events
            .observe(on: UIScheduler())
            .observeValues { [weak self] event in
                guard let strongSelf = self else { return }
                
                switch event {
                case .completed:
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                    
                case .failed(let error):
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                    strongSelf.showSmart24Error(error: error)
                    
                case .interrupted:
                    strongSelf.showAlertMessage(title: nil, message: Constants.closeCallMessage)
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                    
                default:
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                }
        }
        
        loginViewModel.errorSignalProducer
            .observe(on: UIScheduler())
            .start { [weak self] event in
                guard let strongSelf = self else { return }
                
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
        }
    }
    
    func loginWithSocial(type: Int, id: String) {
        loginViewModel.loginWithSocial(type: type, id: id)
            .observe(on: UIScheduler())
            .startWithSignal { signal, disposable in
                
                signal.observeCompleted {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
                signal.observeFailed{ error in
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showSmart24Error(error: error)
                }
        }
    }
}

//MARK:- UITextFieldDelegate delegate
extension LoginViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool  {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= Constants.phoneCharactersCount
    }
}

//MARK:- vk delegate
extension LoginViewController: VKDelegate {
    func vkWillAuthorize() -> Set<VK.Scope> {
        return Constants.vkScope
    }
    
    func vkDidAuthorizeWith(parameters: Dictionary<String, String>) {
        if let userId = parameters[VK.Arg.userId.rawValue] {
            
            loginViewModel.loginWithSocial(type: SocialType.vk.rawValue, id: userId)
                .observe(on: UIScheduler())
                .startWithSignal { signal, disposable in
                    
                    signal.observeCompleted {
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                    
                    signal.observeInterrupted {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.showAlertMessage(title: "Ошибка", message: "Не удалось авторизироваться")
                    }
                    
                    // if user not registered then start registration
                    signal.observeFailed{ error in
                        self.loginViewModel.loginWithVkontakte(userId: parameters[VK.Arg.userId.rawValue])
                            .startWithSignal { [weak self] signal, _ in
                                signal.observeCompleted {
                                    self?.loginWithSocial(type: SocialType.vk.rawValue, id: userId)
                                }
                        }
                    }
            }
        }
    }
    
    func vkAutorizationFailedWith(error: AuthError) {
        DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
            guard let strongSelf = self else { return }
            
            MBProgressHUD.hide(for: strongSelf.view, animated: true)
            debugPrint(error)
        }
    }
    
    func vkDidUnauthorize() {
        debugPrint("vkDidUnauthorize")
    }
    
    func vkShouldUseTokenPath() -> String? {
        return nil
    }
    
    func vkWillPresentView() -> UIViewController {
        return self
    }
}

//MARK:- google+ delegate
extension LoginViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if user == nil {
            MBProgressHUD.hide(for: self.view, animated: true)
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        loginViewModel.loginWithSocial(type: SocialType.googlePlus.rawValue, id: user.userID)
            .observe(on: UIScheduler())
            .startWithSignal { signal, disposable in
                
                signal.observeCompleted {
                    MBProgressHUD.hide(for: self.view, animated: true)
                }
                
                signal.observeInterrupted {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    self.showAlertMessage(title: "Ошибка", message: "Не удалось получить данные с сервера")
                }
                
                // if user not registered then start registration
                signal.observeFailed{ [weak self] error in
                    guard let userImage = user.profile.imageURL(withDimension: 100) else {
                        return
                    }
                    self?.loginViewModel.registrationWithSocial(photo: String(describing: userImage),
                                                                username: user.profile.name,
                                                                type: SocialType.googlePlus.rawValue,
                                                                id: user.userID)
                        .startWithSignal { [weak self] signal, _ in
                            signal.observeCompleted {
                                self?.loginWithSocial(type: SocialType.googlePlus.rawValue, id: user.userID)
                            }
                    }
                }
        }
        
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

extension LoginViewController: GIDSignInUIDelegate {
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        present(viewController, animated: true, completion: nil)
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}
