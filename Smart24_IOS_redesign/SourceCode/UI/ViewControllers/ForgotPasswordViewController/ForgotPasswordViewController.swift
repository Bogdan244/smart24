//
//  ForgotPasswordViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/16/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift
import MBProgressHUD

class ForgotPasswordViewController: UIViewController {
    
    @IBOutlet weak var phoneTextField: LoginTextField!
    @IBOutlet weak var sendButton: LoginButton!
    
    private let forgotPasswordViewModel = ForgotPasswordViewModel()
    
    @IBAction func rememberedButtonAction(_ sender: Any) {
        forgotPasswordViewModel.backToPreviousVC()
    }
    
    @IBAction func signUpButtonAction(_ sender: Any) {
        forgotPasswordViewModel.openRegistrationVC()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
        configureSignals()
    }
    
    private func configureViews() {
        hideKeyboardWhenTappedAround()
        title = "Восстановление пароля"
        
        let phoneImageView          = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 15))
        phoneImageView.image        = R.image.gray_phone_icon()
        phoneImageView.contentMode  = .scaleAspectFit
        phoneTextField.leftView     = phoneImageView
        phoneTextField.leftViewMode = .always
        phoneTextField.keyboardType = .numberPad
    }
    
    private func configureSignals() {
        sendButton.addTarget(forgotPasswordViewModel.cocoaActionForgotPassword, action: CocoaAction<Any>.selector, for: .touchUpInside)
       
        let phoneProducer = forgotPasswordViewModel.racTextProducer(textField: phoneTextField)
        
        forgotPasswordViewModel.phoneProperty   <~ phoneProducer
        sendButton.reactive.isEnabled           <~ forgotPasswordViewModel.phoneSignalProducer
        
        forgotPasswordViewModel.forgotPasswordSignalProducer
            .events
            .observe(on: UIScheduler())
            .observeValues{ [weak self] (event) in
                guard let strongSelf = self else { return }
                
                switch event {
                case .completed:
                    strongSelf.showCodeMessage()
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                    
                case .failed(let error):
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                    strongSelf.showSmart24Error(error: error)
                    
                default:
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                }
        }
    }
    
    private func showCodeMessage() {
        
        var codeTextField: UITextField?
        let smsAlert = UIAlertController(title: "Введите код из SMS", message: nil, preferredStyle: .alert)
        smsAlert.addAction(UIAlertAction(title: "Отменить", style: .cancel))
        smsAlert.addAction(UIAlertAction(title: "OK", style: .default) { [weak self] action in
            self?.view.endEditing(true)
            self?.confirmSMSCode(code: codeTextField?.text)
        })
        
        smsAlert.addTextField { textField in
            codeTextField = textField
        }
        
        present(smsAlert, animated: true, completion: nil)
    }
    
    private func confirmSMSCode(code: String?) {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        forgotPasswordViewModel.forgotPassword(code: code)
            .observe(on: UIScheduler())
            .start { [weak self] event in
                guard let strongSelf = self else { return }
                
                switch event {
                case .completed:
                    strongSelf.loginUser(code: code)
                    
                case .failed(let error):
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                    strongSelf.showSmart24Error(error: error)
                    
                case .interrupted:
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                    strongSelf.showAlertMessage(title: "Ошибка", message: "Не удалось получить данные с сервера")
                    
                default:
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                }
        }
    }
    
    private func loginUser(code: String?) {
        guard let code = code else {
            return
        }

        forgotPasswordViewModel.loginUser(password: code)
            .observe(on: UIScheduler())
            .start { [weak self] event in
                guard let strongSelf = self else { return }
                
                switch event {
                case .failed(let error):
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                    strongSelf.showSmart24Error(error: error)
                    
                case .interrupted:
                    strongSelf.showAlertMessage(title: nil, message: Constants.closeCallMessage)
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                    
                default:
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                }
        }
    }
}
