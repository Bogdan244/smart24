//
//  PhoneTabBarController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/9/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class PhoneTabBarController: UIViewController {
    
    // The custom UIPageControl
    @IBOutlet weak var pageControl: UIPageControl!
    
    var previousIndex: Int?
    var pendingIndex = 1
    var currentIndex = 0
    var pagesArray  = [UIViewController]()
    var pageContainer: UIPageViewController!
    
    let firstPage   = R.storyboard.main.howItWorkContentPageController()
    let secondPage  = R.storyboard.main.howItWorkContentPageController()
    let thirdPage   = R.storyboard.main.howItWorkContentPageController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pagesArray.append(firstPage!)
        pagesArray.append(secondPage!)
        pagesArray.append(thirdPage!)
        
        firstPage?.set(type: .phoneRegistration)
        secondPage?.set(type: .phoneCall)
        thirdPage?.set(type: .phoneSettings)
        
        firstPage?.phoneTabBarController = self
        secondPage?.phoneTabBarController = self
        thirdPage?.phoneTabBarController = self
        
        pageContainer = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        pageContainer.delegate = self
        pageContainer.dataSource = self
        pageContainer.setViewControllers([pagesArray.first!], direction: UIPageViewControllerNavigationDirection.forward, animated: false, completion: nil)
        view.addSubview(pageContainer.view)
        
        view.bringSubview(toFront: pageControl)
        pageControl.numberOfPages = pagesArray.count
        pageControl.currentPage = 0
        pendingIndex = 1
        
    }
    
    func openNextPage() {
        pageContainer.setViewControllers([pagesArray[pendingIndex]], direction: .forward, animated: true, completion: nil )
        pageControl.currentPage = pendingIndex
        currentIndex = pendingIndex
        pendingIndex = pendingIndex + 1
        
        previousIndex = currentIndex == 0 ? nil : currentIndex - 1

    }
    
    func openPreviousPage() {
        if let index = previousIndex {
            pageContainer.setViewControllers([pagesArray[index]], direction: .reverse, animated: true, completion: nil )
            pageControl.currentPage = index
            currentIndex = index
            pendingIndex = index + 1
            
            previousIndex = currentIndex == 0 ? nil : currentIndex - 1
        }
    }
}

// MARK: - UIPageViewController delegates
extension PhoneTabBarController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if let index = pagesArray.index(of: pendingViewControllers.first!) {
            pendingIndex = index
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed {
            currentIndex = pendingIndex
            pageControl.currentPage = currentIndex
        }
    }
}

// MARK: UIPageViewControllerDataSource
extension PhoneTabBarController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex = pagesArray.index(of: viewController)!
        if currentIndex == 0 {
            return nil
        }
        previousIndex = abs((currentIndex - 1) % pagesArray.count)
        
        return pagesArray[previousIndex!]
    }
    
    func pageViewController(_ viewControllerBeforepageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = pagesArray.index(of: viewController)!
        if currentIndex == pagesArray.count - 1 {
            return nil
        }
        pendingIndex = abs((currentIndex + 1) % pagesArray.count)
        previousIndex = abs((currentIndex) % pagesArray.count)
        
        return pagesArray[pendingIndex]
    }
}

