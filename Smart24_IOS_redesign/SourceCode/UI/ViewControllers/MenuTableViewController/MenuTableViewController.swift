//
//  MenuTableViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/18/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {
    
    //MARK:- private properties
    private var user: UserEntity?
    
    private let headerView  = MenuHeaderView.instanciateFromNib()
    private let footerView  = MenuFooterView.instanciateFromNib()
    
    private let viewModel   = GeneralViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureTableHeaders()
        
        if let navigationController = revealViewController().frontViewController as? Smart24NavigationController,
            let mainViewController = navigationController.topViewController as? MainViewController {
            
            mainViewController.openMenuButton.setImage(R.image.close_menu_icon(), for: .normal)
        }
        
        revealViewController().panGestureRecognizer().isEnabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let navigationController = revealViewController().frontViewController as? Smart24NavigationController,
            let mainViewController = navigationController.topViewController as? MainViewController {
            
            mainViewController.openMenuButton.setImage(R.image.menu_icon(), for: .normal)
        }
    }
    
    private func configureTableView() {
        tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0)
        tableView.backgroundView =  UIImageView(image: R.image.menu_background())
        
        tableView.backgroundColor = UIColor.black
        tableView.register(MainMenuCell.self)
        tableView.register(ReferenceMenuCell.self)
        tableView.tableHeaderView = headerView
        tableView.tableFooterView = footerView
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        tableView.rowHeight = 50
    }
    
    private func configureTableHeaders() {
        user = viewModel.getSelfEntity()
        
        headerView.settingsButton.addTarget(self, action: #selector(openSettings), for: .touchUpInside)
        headerView.updateViewWithModel(model: user)
    }
    
    func openSettings() {
        viewModel.openChangeDataVC()
        revealViewController().revealToggle(animated: false)
        revealViewController().panGestureRecognizer().isEnabled = false
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 4 : 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell: MainMenuCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.selectionStyle = .none
            
            switch indexPath.row {
            case 0:
                cell.nameLabel.text = "Профиль"
                cell.leftImageView.image = R.image.white_user_icon()
            case 1:
                cell.nameLabel.text = "Резервная копия"
                cell.leftImageView.image = R.image.white_shield_icon()
            case 2:
                cell.nameLabel.text = "Сделай сам"
                cell.leftImageView.image = R.image.white_create_icon()
            case 3:
                cell.nameLabel.text = "Как это работает"
                cell.leftImageView.image = R.image.white_work_icon()
            default:
                break
            }
            return cell
            
        } else {
            let cell: ReferenceMenuCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
            cell.selectionStyle = .none
            
            switch indexPath.row {
            case 0:
                cell.nameLabel.text = "Утилита"
                cell.rightImageView.image = R.image.utils_icon()
                cell.layer.addBorder(edge: .top, color: .gray, thickness: 0.5, opacity: 0.5)
            case 1:
                cell.nameLabel.text = "Разработчики"
                cell.rightImageView.image = R.image.developers_icon()
            case 2:
                cell.nameLabel.text = "Выход"
                cell.rightImageView.image = R.image.exit_icon()
                cell.layer.addBorder(edge: .bottom, color: .gray, thickness: 0.5, opacity: 0.5)
            default:
                break
            }
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 50 : 40
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
        cell.contentView.backgroundColor = .clear
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                viewModel.openProfileVC()
            case 1:
                viewModel.openBackupVC()
            case 2:
                viewModel.openNewsVC()
            case 3:
                viewModel.openHowItWorkTBC()
            default:
                break
            }
            
        } else {
            switch indexPath.row {
            case 0:
                viewModel.openUtilityVC()
            case 1:
                viewModel.openDevelopersVC()
                break
            case 2:
                viewModel.presentLoginVC()
            default:
                break
            }
            
        }
        
        revealViewController().revealToggle(animated: false)
        revealViewController().panGestureRecognizer().isEnabled = false
    }
}
