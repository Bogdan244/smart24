//
//  VideoViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 1/5/17.
//  Copyright © 2017 Vitya. All rights reserved.
//

import UIKit
import VideoCore
import ReactiveSwift
import MBProgressHUD

class VideoViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var changeCameraButton: UIButton!
    @IBOutlet weak var muteButton: UIButton!
    @IBOutlet weak var videoButton: UIButton!
    
    var streamingVideoSession: VCSimpleSession!
    var activeCall: VSLCall?
    
    private let chatViewModel = ChatViewModel()
    private var startCall = false
    private var account: VSLAccount?
    private var callManager: VSLCallManager?
    private let closeChatAlertView  = CloseChatAlertView()
    
    @IBAction func makeCallButtonAction(_ sender: Any) {
        if startCall {
            callButton.setImage(R.image.make_call_icon(), for: .normal)
            startCall = false
            callManager?.endAllCalls()
        } else {
            callButton.setImage(R.image.close_video_icon(), for: .normal)
            startCall = true
            makeCall()
        }
    }
    
    @IBAction func changeCameraButtonAction(_ sender: Any) {
        if streamingVideoSession.cameraState == .front {
            streamingVideoSession.cameraState = .back
        } else {
            streamingVideoSession.cameraState = .front
        }
    }
    
    @IBAction func muteButtonAction(_ sender: Any) {
        muteSIP()
        /*
         if streamingVideoSession.micGain == Constants.videoMicrophoneOff {
         streamingVideoSession.micGain = Constants.videoMicrophoneOn
         muteButton.setImage(R.image.video_mic_icon(), for: .normal)
         } else {
         streamingVideoSession.micGain = Constants.videoMicrophoneOff
         muteButton.setImage(R.image.video_unmute_mic_icon(), for: .normal)
         } */
    }
    
    @IBAction func videoButtonAction(_ sender: Any) {
        showCloseChatAlert()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        configureSIP()
        configureSignals()
    }
    
    private func configureView() {
        hideKeyboardWhenTappedAround()
        
        DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
            self?.navigationController?.setNavigationBarHidden(true, animated: false)
        }
        
        if let chatVC = childViewControllers.first as? ChatViewController {
            chatVC.collectionView.backgroundColor = .clear
            chatVC.view.backgroundColor = .clear
            containerView.backgroundColor = .clear
        }
    }
    
    private func configureSIP() {
        callManager = VialerSIPLib.sharedInstance().callManager
        if let account = VialerSIPLib.sharedInstance().accounts()?.first as? VSLAccount {
            self.account = account
        }
        
        if let _ = activeCall {
            callButton.setImage(R.image.close_video_icon(), for: .normal)
            startCall = true
        }
    }
    
    private func configureSignals() {
        //MARK:- doesn't send dialogId if it equalse 0
        var dialogId = chatViewModel.dialogId
        if chatViewModel.dialogId == Constants.emptyDialogId {
            dialogId = nil
        }
        
        chatViewModel.sendMessage(message: Constants.startVideoMessage, dialog_id: dialogId, isVideo: IsVideoStart.start.rawValue)
            .observe(on: UIScheduler())
            .start { [weak self] events in
                if events.isCompleted {
                    self?.configureRTMP()
                } else {
                    self?.showAlertMessage(title: nil, message: Constants.cantStartVideoMessage)
                }
        }
    }
    
    private func configureRTMP() {
        DispatchQueue.main.asyncAfter(deadline: .now()) { [weak self] in
            guard let strongSelf = self else { return }
            
            let videoCaptureSize = CGSize(width: 320, height: 240)
            strongSelf.streamingVideoSession = VCSimpleSession(videoSize: videoCaptureSize,
                                                               frameRate: 24, bitrate: 350000,
                                                               useInterfaceOrientation: true,
                                                               cameraState: VCCameraState.front,
                                                               aspectMode: VCAspectMode.ascpectModeFill)
            strongSelf.streamingVideoSession.orientationLocked = false
            strongSelf.streamingVideoSession.useAdaptiveBitrate = false
            strongSelf.streamingVideoSession.delegate = strongSelf
            strongSelf.view.addSubview(strongSelf.streamingVideoSession.previewView)
            strongSelf.view.sendSubview(toBack: strongSelf.streamingVideoSession.previewView)
            strongSelf.streamingVideoSession.previewView.frame = strongSelf.view.bounds
            
            let stringUserId = String(strongSelf.chatViewModel.currentId)
            strongSelf.streamingVideoSession.startRtmpSession(withURL: Constants.rtmpUrl, andStreamKey: stringUserId)
        }
    }
    
    private func showCloseChatAlert() {
        let alert = UIAlertController(title: title, message: Constants.closeChatMessage, preferredStyle: .alert)
        let closeVideo = UIAlertAction(title: "Продолжить чат", style: .default) { [weak self] action in
            self?.chatViewModel.presentChatVC()
        }
        let closeAll = UIAlertAction(title: "Завершить", style: .default) { [weak self] action in
            self?.closeButtonTapped()
        }
        alert.addAction(closeVideo)
        alert.addAction(closeAll)
        alert.view.superview?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(alertClose)))
        present(alert, animated: true, completion: nil)
    }
    
    func alertClose() {
        dismiss(animated: true, completion: nil)
    }
    
    func closeButtonTapped() {
        guard let _ = chatViewModel.dialogId else {
            showAlertMessage(title: nil, message: Constants.notExixstChatMessage)
            return
        }
        
        closeChatAlertView.show()
        view.endEditing(true)
        closeChatAlertView.sendButton.addTarget(self, action: #selector(sendCloseDialog), for: .touchUpInside)
    }
    
    func sendCloseDialog() {
        closeChatAlertView.closeButtonAction(self)
        
        guard let dialogId = chatViewModel.dialogId else {
            return
        }
        let chatGrade = closeChatAlertView.currentGrade.rawValue
        let opinion = closeChatAlertView.getMessage()
        
        MBProgressHUD.showAdded(to: view, animated: true)
        
        chatViewModel.closeDialog(dialog_id: String(dialogId), grade: chatGrade, opinion: opinion)
            .observe(on: UIScheduler())
            .start { [weak self] events in
                guard let strongSelf = self else { return }
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                
                strongSelf.chatViewModel.openRootVC()
        }
    }
    
    private func makeCall() {
        guard let account = account, let callNumber = chatViewModel.expertCallId else { return }
        
        callManager?.startCall(toNumber: callNumber, for: account ) { [weak self] call, error in
            if let error = error {
                debugPrint("Could not start call \(error)")
            } else {
                self?.activeCall = call
                debugPrint("success call")
            }
        }
    }
    
    private func muteSIP() {
        guard let call = activeCall, call.callState != .disconnected else { return }
        
        if call.muted {
            muteButton.setImage(R.image.video_mic_icon(), for: .normal)
        } else {
            muteButton.setImage(R.image.video_unmute_mic_icon(), for: .normal)
        }
        
        callManager?.toggleMute(for: call) { error in
            if error != nil {
                debugPrint("Error muting call: \(error)")
            }
        }
    }
    
    deinit {
        if streamingVideoSession != nil {
            streamingVideoSession.endRtmpSession()
        }
        
        callManager?.endAllCalls()
    }
}

extension VideoViewController: VCSessionDelegate {
    //MARK - VCSessionDelegate
    func connectionStatusChanged(_ sessionState: VCSessionState) {
        switch sessionState {
        case .none:
            break;
        case .previewStarted:
            break;
        case .starting:
            break
        case .started:
            streamingVideoSession.micGain = Constants.videoMicrophoneOff
            break
        case .ended:
            break
        case .error:
            if streamingVideoSession != nil {
                streamingVideoSession.endRtmpSession()
            }
        }
    }
}
