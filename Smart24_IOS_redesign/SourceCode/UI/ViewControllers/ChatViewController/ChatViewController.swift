//
//  ChatViewController.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/2/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import MBProgressHUD
import ReactiveCocoa
import ReactiveSwift
import Kingfisher
import SwiftKeychainWrapper
import Reachability

class ChatViewController: JSQMessagesViewController {
    
    var outgoingBubbleImageView: JSQMessagesBubbleImage!
    var incomingBubbleImageView: JSQMessagesBubbleImage!
    
    private let chatViewModel = ChatViewModel()
    
    private let reach = Reachability.forInternetConnection()
    private let refresher           = UIRefreshControl()
    private let closeChatAlertView  = CloseChatAlertView()
    private var closeChatButton     = UIBarButtonItem()
    private var openVideoButton     = UIBarButtonItem()
    private var openCallButton      = UIBarButtonItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureMessagesView()
        configureNavigationViewController()
        configureSignals()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        loadMessages()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    private func configureMessagesView() {
        hideKeyboardWhenTappedAround()
        title = "Центр поддержки"
        
        senderId = String(chatViewModel.currentId)
        senderDisplayName = ""
        
        refresher.attributedTitle = NSAttributedString(string: "Обновление данных");
        refresher.addTarget(self, action: #selector(loadMessages), for: .valueChanged)
        collectionView.addSubview(refresher);
        
        self.inputToolbar.contentView.leftBarButtonItem = nil
        
        collectionView.showsVerticalScrollIndicator = false;
        collectionView.showsHorizontalScrollIndicator = false;
        
        collectionView?.collectionViewLayout.incomingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
        collectionView?.collectionViewLayout.outgoingAvatarViewSize = CGSize(width: kJSQMessagesCollectionViewAvatarSizeDefault, height:kJSQMessagesCollectionViewAvatarSizeDefault )
        
        setupBubbles()
        
        closeChatButton = UIBarButtonItem(image: R.image.close_menu_icon(), style: .plain , target: self, action: #selector(closeButtonTapped))
        openVideoButton = UIBarButtonItem(image: R.image.white_camera_icon(), style: .plain , target: self, action: #selector(videoButtonTapped))
        openCallButton = UIBarButtonItem(image: R.image.white_phone_icon(), style: .plain , target: self, action: #selector(callButtonTapped))
    }
    
    private func configureSignals() {
        chatViewModel.messages
            .signal
            .observe(on: UIScheduler())
            .observeValues { [weak self] event in
                self?.collectionView.reloadData()
        }
    }
    
    private func configureNavigationViewController() {
        if let dialogId = chatViewModel.dialogId, dialogId != Constants.emptyDialogId {
            navigationItem.rightBarButtonItems = [openVideoButton, openCallButton, closeChatButton]
        } else {
            navigationItem.rightBarButtonItems = [openVideoButton, openCallButton]
        }
    }
    
    func closeButtonTapped() {
        guard let _ = chatViewModel.dialogId else {
            showAlertMessage(title: nil, message: Constants.notExixstChatMessage)
            return
        }
        
        closeChatAlertView.show()
        view.endEditing(true)
        closeChatAlertView.sendButton.addTarget(self, action: #selector(sendCloseDialog), for: .touchUpInside)
    }
    
    func videoButtonTapped() {
        if let isReach = reach?.isReachable(), isReach {
            chatViewModel.openVideoVC(call: nil)
        } else {
            showAlertMessage(title: nil, message: Constants.unreachableCreateBackup)
        }
    }
    
    func callButtonTapped() {
        guard let isReach = reach?.isReachable(), isReach else {
            showAlertMessage(title: nil, message: Constants.unreachableCreateBackup)
            return
        }
        
        if let expertCallId = chatViewModel.expertCallId, expertCallId != "" {
            chatViewModel.openMakeCallVC(callId: expertCallId)
        } else {
            chatViewModel.openMakeCallVC(callId: nil)
        }
    }
    
    func sendCloseDialog() {
        closeChatAlertView.closeButtonAction(self)
        
        guard let dialogId = chatViewModel.dialogId else {
            return
        }
        let chatGrade = closeChatAlertView.currentGrade.rawValue
        let opinion = closeChatAlertView.getMessage()
        
        MBProgressHUD.showAdded(to: view, animated: true)
        
        chatViewModel.closeDialog(dialog_id: String(dialogId), grade: chatGrade, opinion: opinion)
            .observe(on: UIScheduler())
            .startWithSignal { [weak self] signal, disposable in
                guard let strongSelf = self else {
                    return
                }
                
                signal.observeCompleted {
                    strongSelf.configureNavigationViewController()
                    strongSelf.showAlertMessage(title: nil, message: Constants.dialogClosedSuccess)
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                }
                
                signal.observeFailed{ error in
                    strongSelf.showSmart24Error(error: error)
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                }
                
                signal.observeInterrupted {
                    MBProgressHUD.hide(for: strongSelf.view, animated: true)
                }
        }
    }
    
    func loadMessages(isVideo: Int = IsVideoStart.not.rawValue, getDataFromDB: Bool = true) {
        guard let dialogId = chatViewModel.dialogId else {
            return
        }
        
        chatViewModel.getAnswer(dialog_id: dialogId, timestamp: chatViewModel.dialogTimestamp, isVideo: isVideo, getDataFromDB: getDataFromDB)
            .producer
            .observe(on: UIScheduler())
            .start { [weak self] events in
                guard let strongSelf = self else { return }
                
                if let errorCode = events.error?.code,
                    errorCode == Constants.nodialogError,
                    dialogId != Constants.emptyDialogId {
                    
                    strongSelf.showAlertMessage(title: nil, message: "Диалог был завершен оператором")
                }
                
                MBProgressHUD.hide(for: strongSelf.view, animated: true)
                strongSelf.refresher.endRefreshing()
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return chatViewModel.messages.value?[indexPath.row]
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return chatViewModel.messages.value?.count ?? 0
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = chatViewModel.messages.value?[indexPath.row]
        if message?.senderId == String(chatViewModel.currentId) {
            return outgoingBubbleImageView
        } else {
            return incomingBubbleImageView
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView, avatarImageDataForItemAt indexPath: IndexPath) -> JSQMessageAvatarImageDataSource? {
        return JSQMessagesAvatarImageFactory.avatarImage(with: R.image.white_user_icon(), diameter: 15)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForCellTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        let jsqmessage = chatViewModel.messages.value?[indexPath.row]
        return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: jsqmessage?.date)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        cell.textView.textColor = UIColor.black
        
        if let expertAvatarUrl = chatViewModel.messages.value?[indexPath.row].expert_avatar,
            let userAvatarUrl = URL(string: expertAvatarUrl) {
            
            cell.avatarImageView.sizeThatFits(CGSize(width: 25, height: 25))
            cell.avatarImageView.roundedView(border: false, borderColor: nil, borderWidth: nil, cornerRadius: nil)
            cell.avatarImageView.kf.setImage(with: ImageResource(downloadURL: userAvatarUrl, cacheKey: nil),
                                             placeholder: nil,
                                             options: nil,
                                             progressBlock: nil,
                                             completionHandler: nil)
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        view.endEditing(true)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!,
                                 layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!,
                                 heightForCellTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return indexPath.row % 3 == 0 ? 20 : 0
    }
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        if text.isEmpty {
            return
        }
        
        button.isEnabled = false
        
        //MARK:- doesn't send dialogId if it equalse 0
        var dialogId = chatViewModel.dialogId
        if chatViewModel.dialogId == Constants.emptyDialogId {
            dialogId = nil
        }
        
        chatViewModel.sendMessage(message: text, dialog_id: dialogId, isVideo: nil)
            .observe(on: UIScheduler())
            .startWithSignal { [weak self] signal, disposable in
                signal.observeValues { [weak self] value in
                    
                    button.isEnabled = true
                    self?.configureNavigationViewController()
                    self?.collectionView.reloadData()
                    self?.finishSendingMessage(animated: true)
                }
        }
    }
    
    private func setupBubbles() {
        let factory = JSQMessagesBubbleImageFactory()
        
        outgoingBubbleImageView = factory?.outgoingMessagesBubbleImage(with: UIColor(named: .ChatMessageColor))
        incomingBubbleImageView = factory?.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
}
