//
//  MainMenuCell.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/23/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class MainMenuCell: UITableViewCell, ReusableView, NibLoadableView {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var leftImageView: UIImageView!
}
