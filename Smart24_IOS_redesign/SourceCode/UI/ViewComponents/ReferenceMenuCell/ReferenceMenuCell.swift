//
//  ReferenceMenuCell.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/23/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

class ReferenceMenuCell: UITableViewCell, ReusableView, NibLoadableView {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var rightImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = .clear
    }
}
