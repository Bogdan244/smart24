//
//  BackupViewModel.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/1/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift
import SwiftKeychainWrapper
import AddressBook
import iCloudDocumentSync
import Contacts
import enum Result.NoError

class BackupViewModel: GeneralViewModel {
    
    var allContacts: CFArray?
    var userContacts = [CNContact]()
    
    private let query = NSMetadataQuery()
    
    override func configureSignals() {
        getContacts()
    }
    
    func getInfoBackup() -> SignalProducer<Int, NoError> {
        
        return SignalProducer<Int, NoError> { [weak self]
            observer, disposable in
            
            guard let strongSelf = self else { return }
            let currentUserId = String(describing: strongSelf.currentId)
            
            networkProvider.request(.getInfoBackup(currentUserId)) { result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            debugPrint(json)
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                guard let isBackup = json[MapperKey.isbackup] as? String, isBackup == IsBackupExist.exist.rawValue,
                                    let date = json[MapperKey.date] as? Int else {
                                        debugPrint("backup not exist")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                KeychainWrapper.standard.set(date, forKey: Constants.lastBackupDate)
                                
                                observer.send(value: date)
                            } else {
                                debugPrint("error status")
                                observer.sendInterrupted()
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func putInfoBackup(isBackup: Bool, date: Int) -> SignalProducer<(), NoError> {
        
        return SignalProducer<(), NoError> { [weak self]
            observer, disposable in
            
            guard let strongSelf = self else { return }
            let currentUserId = String(describing: strongSelf.currentId)
            
            networkProvider.request(.putInfoBackup(currentUserId, isBackup, date)) { result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String: AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            debugPrint(json)
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
                                debugPrint("success sended data")
                                observer.sendCompleted()
                                
                            } else {
                                debugPrint("error status")
                                observer.sendInterrupted()
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    // retrieve contacts and call backup method
    func fetchAddressBookContacts(fetchType: BackupState) -> SignalProducer<(), CloudSyncType> {
        return SignalProducer<(), CloudSyncType> { [weak self]
            observer, disposable in
            
            var addressBook: ABAddressBook?
            if let book = ABAddressBookCreateWithOptions(nil, nil) {
                addressBook = book.takeRetainedValue()
            } else {
                observer.sendInterrupted()
                return
            }
            
            self?.allContacts = ABAddressBookCopyArrayOfAllPeople(addressBook).takeRetainedValue() as CFArray
            debugPrint(self?.allContacts ?? "contacts is nil")
            
            if CFArrayGetCount(self?.allContacts) > 0 {
                if fetchType == .createBackup, let allContacts = self?.allContacts {
                    self?.createBackupAddressBook(contacts: allContacts)
                        .startWithSignal { signal, _ in
                            signal.observeCompleted {
                                observer.sendCompleted()
                            }
                            
                            signal.observeFailed { error in
                                observer.send(error: error)
                            }
                            
                            signal.observeInterrupted {
                                observer.sendInterrupted()
                            }
                    }
                }
            } else {
                observer.send(error: .noContacts)
            }
        }
    }
    
    // MARK: iCloudDocumentSync
    
    func createBackupAddressBook(contacts: CFArray) -> SignalProducer<(), CloudSyncType> {
        return SignalProducer<(), CloudSyncType> { [weak self]
            observer, disposable in
            
            debugPrint(iCloud.shared().ubiquitousDocumentsDirectoryURL())
            
            if iCloud.shared().checkUbiquityContainer() {
                
                let backupTimeInterval = Int64(NSDate().timeIntervalSince1970)
                let cloudFileName = CFArrayGetCount(contacts) < 10 ? "addressBook.0\(CFArrayGetCount(contacts)).\(backupTimeInterval)" : "addressBook.\(CFArrayGetCount(contacts)).\(backupTimeInterval)"
                
                iCloud.shared().saveAndCloseDocument(withName: cloudFileName,
                                                     withContent: ABPersonCreateVCardRepresentationWithPeople(contacts).takeRetainedValue() as Data) {
                                                        [weak self] (document, data, error) in
                                                        guard let strongSelf = self else { return }
                                                        
                                                        if error == nil {
                                                            debugPrint("Document saved \(document?.fileURL)")
                                                            
                                                            strongSelf.saveBackupDate(date: Int(backupTimeInterval))
                                                                .startWithSignal { signal, _ in
                                                                    signal.observeCompleted {
                                                                        observer.sendCompleted()
                                                                    }
                                                                    
                                                                    signal.observeFailed{ error in
                                                                        observer.send(error: error)
                                                                    }
                                                                    
                                                                    signal.observeInterrupted {
                                                                        observer.sendInterrupted()
                                                                    }
                                                            }
                                                            
                                                            
                                                        } else {
                                                            DispatchQueue.main.async {
                                                                debugPrint("Document save Error")
                                                                //strongSelf.errorAlert(alertType: .error)
                                                                observer.send(error: .error)
                                                            }
                                                        }
                }
                
            } else {
                observer.send(error: .unavailable)
            }
        }
    }
    
    private func saveBackupDate(date: Int) -> SignalProducer<(), CloudSyncType> {
        return SignalProducer<(), CloudSyncType> { [weak self]
            observer, disposable in
            
            self?.putInfoBackup(isBackup: true, date: date)
                .startWithSignal { signal, _ in
                    
                    signal.observeCompleted {
                        debugPrint("data was succesfuly saved")
                        observer.sendCompleted()
                    }
                    
                    signal.observeFailed{ _ in
                        observer.send(error: .error)
                    }
                    
                    signal.observeInterrupted {
                        observer.send(error: .error)
                    }
            }
        }
    }
    
    func fileListQuery(pathToFile: URL) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(queryCompleted), name: NSNotification.Name.NSMetadataQueryDidFinishGathering, object: nil)
        
        query.searchScopes = [NSMetadataQueryUbiquitousDocumentsScope]
        query.predicate = NSPredicate(format: "%K like 'address*'", NSMetadataItemFSNameKey)
        query.start()
    }
    
    @objc func queryCompleted(notification: NSNotification) {
        
        query.stop()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.NSMetadataQueryDidFinishGathering, object: nil)
        
        let resultArray = self.query.results
        var filesArray = [String]()
        
        if resultArray.count > 0 {
            
            for index in 0...resultArray.count - 1 {
                if let value = query.value(ofAttribute: NSMetadataItemFSNameKey, forResultAt: index) {
                    filesArray.append(String(describing: value))
                    debugPrint("\(iCloud.shared().ubiquitousDocumentsDirectoryURL()) --- \(value)")
                }
            }
            
            //MARK:- get the oldest date
            filesArray.sort { first, second in
                if let firstElement = first.components(separatedBy: ".").last,
                    let secondElement = second.components(separatedBy: ".").last {
                    return firstElement < secondElement
                }
                return false
            }
            
            self.restoreLastBackupAddressBook(backupName: filesArray.last!)
            
        }
    }
    
    func restoreLastBackupAddressBook(backupName: String) {
        
        if iCloud.shared().checkUbiquityContainer() {
            
            // MBProgressHUD.showAdded(to: view, animated: true)
            
            iCloud.shared().retrieveCloudDocument(withName: backupName) { [weak self] (document, data, error) in
                guard let strongSelf = self else {
                    return
                }
                
                if error == nil {
                    var isSaved = true
                    
                    do {
                        if let data = data {
                            let icloudContacts = try CNContactVCardSerialization.contacts(with: data)
                            
                            icloudContacts.forEach { contact in
                                let isContains = strongSelf.userContacts.contains { $0.givenName == contact.givenName }
                                
                                if !isContains, let mutableContact = contact.mutableCopy() as? CNMutableContact {
                                    let saveRequest = CNSaveRequest()
                                    saveRequest.add(mutableContact, toContainerWithIdentifier: nil)
                                    
                                    do {
                                        let contactStore = CNContactStore()
                                        try contactStore.execute(saveRequest)
                                        debugPrint("Success, You saved users \(contact.givenName)")
                                    } catch let error {
                                        debugPrint("Error = \(error)")
                                        isSaved = false
                                    }
                                }
                            }
                            
                        }
                    } catch let error {
                        debugPrint("error \(error)")
                        isSaved = false
                    }
                    
                    debugPrint("saved \(isSaved)")
                }
            }
        }
    }
    
    func getContacts() {
        let keys = [CNContactGivenNameKey ,CNContactImageDataKey,CNContactPhoneNumbersKey]
        var message = String()
        let contactStore = CNContactStore()
        
        // Get all the containers
        var allContainers: [CNContainer] = []
        do {
            allContainers = try contactStore.containers(matching: nil)
        } catch {
            debugPrint("Error fetching containers")
        }
        
        // Iterate all containers and append their contacts to our results array
        userContacts.removeAll()
        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
            
            do {
                let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keys as [CNKeyDescriptor])
                userContacts.append(contentsOf: containerResults)
                message = "\(userContacts.count)"
            } catch {
                debugPrint("Error fetching results for container")
            }
        }
        
        debugPrint(message)
    }
}
