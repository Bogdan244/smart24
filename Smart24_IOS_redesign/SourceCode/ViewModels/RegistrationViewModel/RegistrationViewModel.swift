//
//  RegistrationViewModel.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/17/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ReactiveSwift
import ReactiveCocoa
import ObjectMapper
import RealmSwift
import SwiftKeychainWrapper
import enum Result.NoError

class RegistrationViewModel: GeneralViewModel {
    
    var cocoaActionRegistration: CocoaAction<Any>!
    var registrationSignalProducer: Action<(), (), ErrorEntity>!
    var inputValidData = MutableProperty<Bool>(false)
    
    let nameProperty            = MutableProperty<String?>("")
    let surnameProperty         = MutableProperty<String?>("")
    let phoneProperty           = MutableProperty<String?>("")
    let passwordProperty        = MutableProperty<String?>("")
    let confirmPasswordProperty = MutableProperty<String?>("")
    
    var nameSignalProducer: SignalProducer<Bool, NoError>!
    var surnameSignalProducer: SignalProducer<Bool, NoError>!
    var phoneSignalProducer: SignalProducer<Bool, NoError>!
    var passwordSignalProducer: SignalProducer<Bool, NoError>!
    var confirmPaswordSignalProducer: SignalProducer<Bool, NoError>!
    
    override func configureSignals() {
        
        // MARK Register Producers
        nameSignalProducer = nameProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.characters.count >= 1 && value.characters.count <= 20 || value == "" else {
                    return false
                }
                return true
        }
        
        surnameSignalProducer = surnameProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.characters.count >= 1 && value.characters.count <= 20 || value == "" else {
                    return false
                }
                return true
        }
        
        phoneSignalProducer = phoneProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.characters.count == 12 || value == "" else {
                    return false
                }
                return true
        }
        
        passwordSignalProducer = passwordProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.characters.count >= 1 && value.characters.count <= 32 || value == "" else {
                    return false
                }
                return true;
        }
        
        confirmPaswordSignalProducer = confirmPasswordProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value == self.passwordProperty.value && value.characters.count >= 1 || value == "" else {
                    return false
                }
                return true
        }
        
        inputValidData <~ SignalProducer.combineLatest([nameSignalProducer, surnameSignalProducer, phoneSignalProducer, passwordSignalProducer, confirmPaswordSignalProducer])
            .flatMap((.latest), transform: { credentionals in
                return SignalProducer<Bool, NoError> {
                    
                    observer, disposable in
                    
                    if self.isTextFieldEmpty() && credentionals[0] && credentionals[1] && credentionals[2] && credentionals[3] && credentionals[4] {
                        observer.send(value: true)
                    } else {
                        observer.send(value: false)
                    }
                    observer.sendCompleted()
                }
            })
        
        // MARK Registration Action
        registrationSignalProducer = Action<(),(), ErrorEntity> { [unowned self] _ in
            return self.registerUser()
        }
        
        cocoaActionRegistration = CocoaAction(registrationSignalProducer, input:())
    }
    
    private func registerUser() -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { [weak self]
            observer, disposable in
            guard let strongSelf = self else { return }
            
            let name            = strongSelf.nameProperty.value!
            let surname         = strongSelf.surnameProperty.value!
            let phone           = strongSelf.phoneProperty.value!
            let password        = strongSelf.passwordProperty.value!
            let confPass        = strongSelf.confirmPasswordProperty.value!
            let phoneModel      = Constants.phoneModel
            
            networkProvider.request(.registration(name, surname, phone, password, confPass, phoneModel)) { result in
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String:AnyObject] {
                            print(json)
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                observer.sendCompleted()
                                
                            } else {
                                guard let jsonArray = json[MapperKey.errors] as? [AnyObject],
                                    let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
                                    let error = errors.first else {
                                        debugPrint("can't parse data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                observer.send(error: error)
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                    
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func loginUser() -> SignalProducer<UserEntity, ErrorEntity> {
        
        return SignalProducer<UserEntity, ErrorEntity> { [weak self]
            observer, disposable in
            guard let strongSelf = self else { return }
            
            let phone    = strongSelf.phoneProperty.value!
            let password = strongSelf.passwordProperty.value!
            
            networkProvider.request(.login(phone, password)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true,
                                let userId = json[MapperKey.uid] as? String {
                                
                                
                                guard let item = Mapper<UserEntity>().map(JSON: json) else {
                                    debugPrint("can't parse data")
                                    return;
                                }
                                
                                
                                //MARK:- remove DB data if it isn't current user
                                if let currentId = self?.currentId, String(currentId) != userId {
                                    do {
                                        let realm = try Realm()
                                        try realm.write {
                                            realm.deleteAll()
                                        }
                                    } catch {
                                        debugPrint("can't delete data from DB")
                                    }
                                }
                                
                                KeychainWrapper.standard.set(userId, forKey: Constants.currentUserId)
                                self?.setDataInDB(data: item)
                                
                                observer.send(value: item)
                                observer.sendCompleted()
                                
                                self?.openContainerVC()
                            } else {
                                guard let jsonArray = json[MapperKey.errors] as? [AnyObject],
                                    let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
                                    let error = errors.first else {
                                        debugPrint("can't parse error data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                debugPrint("error status")
                                observer.send(error: error)
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    debugPrint("error response")
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    private func isTextFieldEmpty() -> Bool {
        if  self.nameProperty.value! != "" &&
            self.surnameProperty.value! != "" &&
            self.phoneProperty.value! != "" &&
            self.passwordProperty.value! != "" &&
            self.confirmPasswordProperty.value! != "" {
            return true
        }
        
        return false
    }
}
