//
//  ForgotPasswordViewModel.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/17/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift
import ObjectMapper
import RealmSwift
import SwiftKeychainWrapper
import enum Result.NoError

class ForgotPasswordViewModel: GeneralViewModel {
    
    let phoneProperty = MutableProperty<String?>("")
    
    var cocoaActionForgotPassword: CocoaAction<Any>!
    var forgotPasswordSignalProducer: Action<(), (), ErrorEntity>!
    var phoneSignalProducer: SignalProducer<Bool, NoError>!
    
    override func configureSignals() {
        phoneSignalProducer = phoneProperty
            .producer
            .map { (text: String?) -> Bool in
                guard let value = text, value.characters.count >= 12 &&  value.characters.count < 15 else {
                    return false
                }
                return true
        }
        
        // MARK forgot password Action
        forgotPasswordSignalProducer = Action<(),(), ErrorEntity> { [unowned self] _ in
            return self.forgotPassword(code: nil)
        }
        
        cocoaActionForgotPassword = CocoaAction(forgotPasswordSignalProducer, input:())
    }
    
    func forgotPassword(code: String?) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { [weak self]
            observer, disposable in
            guard let strongSelf = self else { return }
            
            let phone = strongSelf.phoneProperty.value!
            
            networkProvider.request(.forgotPassword(phone, code)) { result in
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String: AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                observer.sendCompleted()
                            } else {
                                guard let jsonArray = json[MapperKey.errors] as? [AnyObject],
                                    let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
                                    let error = errors.first else {
                                        debugPrint("can't parse data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                observer.send(error: error)
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                    
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func loginUser(password: String) -> SignalProducer<UserEntity, ErrorEntity> {
        
        return SignalProducer<UserEntity, ErrorEntity> { [weak self]
            observer, disposable in
            guard let strongSelf = self else { return }
            
            let phone = strongSelf.phoneProperty.value!
            
            networkProvider.request(.login(phone, password)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String: AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            debugPrint(json)
                            
                            if let status = json[MapperKey.success] as? Bool, status == true,
                                let userId = json[MapperKey.uid] as? String {
                                
                                
                                guard let item = Mapper<UserEntity>().map(JSON: json) else {
                                    debugPrint("can't parse data")
                                    return;
                                }
                                
                                
                                //MARK:- remove DB data if it isn't current user
                                if let currentId = self?.currentId, String(currentId) != userId {
                                    do {
                                        let realm = try Realm()
                                        try realm.write {
                                            realm.deleteAll()
                                        }
                                    } catch {
                                        debugPrint("can't delete data from DB")
                                    }
                                }
                                
                                KeychainWrapper.standard.set(userId, forKey: Constants.currentUserId)
                                self?.setDataInDB(data: item)
                                
                                observer.send(value: item)
                                observer.sendCompleted()
                                
                                self?.openContainerVC()
                            } else {
                                guard let jsonArray = json[MapperKey.errors] as? [AnyObject],
                                    let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
                                    let error = errors.first else {
                                        debugPrint("can't parse error data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                debugPrint("error status")
                                observer.send(error: error)
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    debugPrint("error response")
                    observer.sendInterrupted()
                }
            }
        }
    }
}
