//
//  ChatViewModel.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/2/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift
import RealmSwift
import ObjectMapper
import SwiftKeychainWrapper
import JSQMessagesViewController
import enum Result.NoError

class JSQMessageEntity: JSQMessage {
    
    var expert_avatar: String?
    
    convenience init!(senderId: String!, senderDisplayName: String!, date: Date!, text: String!, expert_avatar: String!) {
        self.init(senderId: senderId, senderDisplayName: senderDisplayName, date: date, text: text)
        self.expert_avatar = expert_avatar
    }
}

class ChatViewModel: GeneralViewModel {
    
    var messages: ReactiveSwift.Property<[JSQMessageEntity]?> { return Property(_messages) }
    
    private var _messages = MutableProperty<[JSQMessageEntity]?>(nil)
    
    func sendMessage(message: String, dialog_id: String?, isVideo: Int?) -> SignalProducer<JSQMessageEntity, NoError> {
        
        return SignalProducer<JSQMessageEntity, NoError> { [weak self]
            observer, disposable in
            guard let strongSelf = self else { return }
            
            networkProvider.request(.sendMessage(String(strongSelf.currentId), message, dialog_id, isVideo)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            debugPrint(json)
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
                                //MARK:- check and parse dialog id
                                var userDialogId = ""
                                if let stringId = json[MapperKey.dialog_id] as? String {
                                    userDialogId = stringId
                                } else if let intId = json[MapperKey.dialog_id] as? Int {
                                    userDialogId = String(intId)
                                }
                                
                                guard let item = Mapper<SendMessageEntity>().map(JSON: json),
                                    let userId = self?.currentId else {
                                        debugPrint("can't parse data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                KeychainWrapper.standard.set(userDialogId, forKey: Constants.dialog_id)
                                KeychainWrapper.standard.set(item.cc_id, forKey: Constants.expertCallId)
                                
                                //MARK:- if message used only for starting video then doesn't add it to chat
                                if let value = isVideo, value == IsVideoStart.start.rawValue {
                                    observer.sendCompleted()
                                    return
                                }
                                
                                let currentUserAvatar = self?.getSelfEntity()?.user_photo
                                
                                guard let jsqMessage = JSQMessageEntity.init(senderId: String(describing: userId),
                                                                             senderDisplayName: "",
                                                                             date: Date(timeIntervalSince1970: Double(item.time_of_receipt - 1)),
                                                                             text: message,
                                                                             expert_avatar: currentUserAvatar) else {
                                                                                observer.sendInterrupted()
                                                                                return
                                }
                                
                                if self?._messages.value == nil {
                                    self?._messages.value = [jsqMessage]
                                    self?.setSelfMessageInDB(message: message, time: item.time_of_receipt - 1, avatar: currentUserAvatar)
                                } else {
                                    self?._messages.value?.append(jsqMessage)
                                    self?.setSelfMessageInDB(message: message, time: item.time_of_receipt - 1, avatar: currentUserAvatar)
                                }
                                
                                observer.send(value: jsqMessage)
                                observer.sendCompleted()
                                
                            } else {
                                debugPrint("error status")
                                observer.sendInterrupted()
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func getAnswer(dialog_id: String, timestamp: Int?, isVideo: Int?, getDataFromDB: Bool = true) -> SignalProducer<JSQMessageEntity, ErrorEntity> {
        
        return SignalProducer<JSQMessageEntity, ErrorEntity> { [weak self]
            observer, disposable in
            guard let strongSelf = self else { return }
            
            if getDataFromDB {
                let messages = strongSelf.getMessages()
                messages?.forEach { element in
                    if let jsqMessage = JSQMessageEntity.init(senderId: element.expert_id,
                                                              senderDisplayName: element.expert_name,
                                                              date: Date(timeIntervalSince1970: Double(element.time)),
                                                              text: element.message,
                                                              expert_avatar: element.expert_avatar) {
                        
                        if strongSelf._messages.value == nil {
                            strongSelf._messages.value = [jsqMessage]
                        } else if let isContains = strongSelf._messages.value?.contains(jsqMessage), isContains == false {
                            strongSelf._messages.value?.append(jsqMessage)
                        }
                    }
                }
            }
            
            networkProvider.request(.getAnswer(String(strongSelf.currentId), dialog_id, timestamp, isVideo)) { [weak self] result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                debugPrint(json)
                                
                                guard let item = Mapper<GetAnswerEntity>().map(JSON: json),
                                    let lastMessageTime = item.messages?.last?.time else {
                                        debugPrint("can't parse data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                KeychainWrapper.standard.set(lastMessageTime, forKey: Constants.timestamp)
                                KeychainWrapper.standard.set(item.cc_id, forKey: Constants.expertCallId)
                                
                                item.messages?.forEach { element in
                                    element.expert_id = String(item.expert_id)
                                    self?.setDataInDB(data: element)
                                    
                                    if let jsqMessage = JSQMessageEntity.init(senderId: element.expert_id,
                                                                              senderDisplayName: element.expert_name,
                                                                              date: Date(timeIntervalSince1970: Double(element.time)),
                                                                              text: element.message,
                                                                              expert_avatar: element.expert_avatar) {
                                        
                                        if self?._messages.value == nil {
                                            self?._messages.value = [jsqMessage]
                                        } else {
                                            self?._messages.value?.append(jsqMessage)
                                        }
                                    }
                                }
                                
                                
                                //MARK:- read new messages
                                self?.readMessages().start()
                                
                                if let lastMessage = self?._messages.value?.last {
                                    observer.send(value: lastMessage)
                                }
                                observer.sendCompleted()
                                
                            } else {
                                guard let jsonArray = json[MapperKey.errors] as? [AnyObject],
                                    let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
                                    let error = errors.first else {
                                        debugPrint("can't parse error data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                //MARK:- remove dialog_id if it's incorrect
                                if Constants.nodialogError == error.code {
                                    debugPrint("getAnswer userDialogId ------\(strongSelf.dialogId)")
                                    KeychainWrapper.standard.set(Constants.emptyDialogId, forKey: Constants.dialog_id)
                                    debugPrint("getAnswer new userDialogId ------\(strongSelf.dialogId)")
                                }
                                
                                observer.send(error: error)
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    func closeDialog(dialog_id: String, grade: String, opinion: String?) -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { [weak self]
            observer, disposable in
            guard let strongSelf = self else { return }
            
            networkProvider.request(.closeDialog(String(strongSelf.currentId), dialog_id, grade, opinion)) { result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            debugPrint(json)
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
                                KeychainWrapper.standard.set(Constants.emptyDialogId, forKey: Constants.dialog_id)
                                
                                //                                do {
                                //                                    let realm = try Realm()
                                //                                    try realm.write {
                                //                                        realm.delete(realm.objects(MessagesEntity.self))
                                //                                    }
                                //                                } catch { debugPrint("can't delete data from DB")}
                                
                                observer.sendCompleted()
                                
                            } else {
                                guard let jsonArray = json[MapperKey.errors] as? [AnyObject],
                                    let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
                                    let error = errors.first else {
                                        debugPrint("can't parse error data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                debugPrint("error status")
                                observer.send(error: error)
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    private func readMessages() -> SignalProducer<(), ErrorEntity> {
        
        return SignalProducer<(), ErrorEntity> { [weak self]
            observer, disposable in
            guard let strongSelf = self else { return }
            
            guard let dialogId = strongSelf.dialogId, let timestamp = strongSelf.dialogTimestamp else {
                debugPrint("can't get dialogId or timestamp")
                return
            }
            
            networkProvider.request(.messagesRead(String(strongSelf.currentId), String(dialogId), timestamp)) { result in
                
                switch(result) {
                case let .success(response):
                    do {
                        if let json: [String:AnyObject] = try response.mapJSON() as? [String: AnyObject] {
                            debugPrint(json)
                            
                            if let status = json[MapperKey.success] as? Bool, status == true {
                                
                                observer.sendCompleted()
                                
                            } else {
                                guard let jsonArray = json[MapperKey.errors] as? [AnyObject],
                                    let errors = Mapper<ErrorEntity>().mapArray(JSONObject: jsonArray),
                                    let error = errors.first else {
                                        debugPrint("can't parse error data")
                                        observer.sendInterrupted()
                                        return
                                }
                                
                                debugPrint("error status")
                                observer.send(error: error)
                            }
                        }
                    } catch {
                        observer.sendInterrupted()
                    }
                case .failure(_):
                    observer.sendInterrupted()
                }
            }
        }
    }
    
    private func getMessages() -> [MessagesEntity]? {
        do {
            let realm = try Realm()
            let messagesEntity = realm.objects(MessagesEntity.self).toArray(ofType: MessagesEntity.self)
            return messagesEntity.count > 0 ? messagesEntity : nil
        } catch {
            return nil
        }
    }
    
    private func setSelfMessageInDB(message: String, time: Int, avatar: String?) {
        let selfMessage = MessagesEntity()
        selfMessage.message = message
        selfMessage.expert_avatar = ""
        selfMessage.time = time
        selfMessage.expert_id = String(currentId)
        
        if let avatar = avatar {
            selfMessage.expert_avatar = avatar
        }
        
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(selfMessage, update: true)
            }
        } catch {
            debugPrint("can't save data in DB")
        }
    }
}
