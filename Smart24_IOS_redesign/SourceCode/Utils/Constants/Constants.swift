//
//  Constants.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 11/16/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import SwiftyVK

enum Constants {
    static let BASE_URL         = "http://so24.net/api/"
    
    //vkontakte
    static let vkId                     = "5740963"
    static let vkScope: Set<VK.Scope>   = [.wall]
    static let vkAvatarSize             = "photo_50"
    
    //odnoklassniki
    static let okAppKey  = ""
    static let okAppId   = ""
    
    //google+
    static let gClientID = "543076381409-6ain4puaacvq2vs9sbgsi7kgghh0kgsd.apps.googleusercontent.com"
    
    //FaceBook 
    static let fbGraphPath = "me"
    static let fbParameters: [String : Any] = ["fields": "id, name, picture"]
    
    static let smart24UA_phone      = "tel://0800600001"
    
    // SIP
    static let sipNumber                = "8000@3cxsystem.smart-24.net"
    static let sipUsername              = "5002"
    static let sipPassword              = "5c31h42"
    static let sipDomain                = "3cxsystem.smart-24.net"
    static let sipProxy                 = "sip:3cxsystem.smart-24.net;lr"
    static let connectDurationInterval  = 1.0
    static let sipCallStateKey          = "callState"
    
    // RTMP
    static let rtmpUrl                     = "rtmp://so24vs1.cloudapp.net/video/"
    static let videoMicrophoneOff: Float   = 0.000001
    static let videoMicrophoneOn: Float    = 1
    static let startVideoMessage           = "start video"
   
    static let firstRun             = "firstRun"
    static let currentUserId        = "id"
    static let lastBackupDate       = "lastBackupDate"
    static let backupPeriod         = "backupPeriod"
    static let setOnBackupPeriod    = "setOnBackupPeriod"
    static let dialog_id            = "dialogId"
    static let emptyDialogId        = "0"
    static let timestamp            = "timestamp"
    static let keyChainValue        = "key"
    static let expertCallId         = "expertCallId"
    
    static let phoneCharacters      = "+0123456789"
    static let phoneCharactersCount = 12
    static let teamViewLink         = "https://www.teamviewer.com/ru/download/ios/"
    static let wezomLink            = "https://wezom.mobi/"
    static let freshDigitalLink     = "http://www.fresh-d.net/"
    static let smartlLink           = "https://so24.net"
    
    // error types
    static let nodialogError        = 31
    static let notAuthError         = 20
    static let userNotFoundError    = 10
    
    static let notificationError        = "Не удалось удалить уведомление, проверте интернет соединение"
    static let notificationsExist       = "Сообщения и уведомления"
    static let notificationsNotExist    = "Сообщения и уведомления отсутствуют."
    static let servicesExist            = "Ваши активные услуги"
    static let servicesNotExist         = "Нет активных услуг."
    
    // messages
    static let unreachableMessage       = "У Вас отключен Интернет. Часть функций будет недоступна!"
    static let unreachableCreateBackup  = "У Вас отключен Интернет. Невозможно сохранить контакты!"
    static let unreachableRestoreBackup = "У Вас отключен Интернет. Невозможно восстановить контакты!"
    static let cantCallSIPMessage       = "Не удалось дозвониться к оператору"
    static let cantStartVideoMessage    = "Не удалось создать видеоконференцию, попробуйте позже."
    static let shialdMessage            = "Ваше устройство защищено! Вы можете управлять устройством и отслеживать его из личного кабинета на сайте  http:/so24.net (должен быть включен Интернет)"
    static let closeCallMessage         = "Не удалось подключится к серверу, проверте интернет соединение."
    static let savedMessage             = "Данные успешно сохранены"
    static let backupErrorMessage       = "Ошибка сохранения данных. \nОблачное хранилище недоступно."
    static let dayBackupMessage         = "Один раз в день"
    static let weekBackupMessage        = "Один раз в неделю"
    static let monthBackupMessage       = "Один раз в месяц"
    static let yearBackupMessage        = "Один раз в год"
    static let backupButtonState        = "backupButtonState"
    static let backupButtonDayIndex     = "backupButtonDayIndex"
    static let backupDate               = "backupDate"
    static let backupDateFormat         = "dd MMM YYYY, H:mm"
    static let profileDateFormat        = "dd.MM.yyyy"
    static let phoneModel               = "iPhone"
    static let notExixstChatMessage     = "У вас нет активных диалогов"
    static let dialogClosedSuccess      = "Диалог был успешно завершен"
    static let closeChatMessage         = "Завершить активный чат с экспертом?"
    
    // ChangeDataViewController
    static let changePhoto              = "Изменить фото"
    static let camera                   = "Камера"
    static let galery                   = "Галерея"
    static let close                    = "Отмена"
    static let editingTitle             = "Редактирование"
}

enum SocialType: Int {
    case googlePlus
    case facebook
    case vk
    case ok
}

enum ChatGrade: String {
    case good = "good"
    case bad = "bad"
}

enum IsBackupExist: String {
    case not = "0"
    case exist = "1"
}

enum SmartDensity: Int {
    case for2x = 7
    case for3x = 12
}

enum IsVideoStart: Int {
    case not
    case start
}
