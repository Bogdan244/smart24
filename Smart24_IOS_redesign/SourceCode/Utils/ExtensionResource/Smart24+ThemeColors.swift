// Generated using SwiftGen, by O.Halligon — https://github.com/AliSoftware/SwiftGen

#if os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIColor
  typealias Color = UIColor
#elseif os(OSX)
  import AppKit.NSColor
  typealias Color = NSColor
#endif

extension Color {
  convenience init(rgbaValue: UInt32) {
    let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
    let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
    let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
    let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0

    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
}

// swiftlint:disable file_length
// swiftlint:disable line_length

// swiftlint:disable type_body_length
enum ColorName {
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#2ecc71"></span>
  /// Alpha: 100% <br/> (0x2ecc71ff)
  case LoginButtonNormal
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#e9e7e5"></span>
  /// Alpha: 100% <br/> (0xe9e7e5ff)
  case LoginTextFiledColor
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#1cb0f6"></span>
  /// Alpha: 100% <br/> (0x1cb0f6ff)
  case BlueProgress
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ddf1d8"></span>
  /// Alpha: 100% <br/> (0xddf1d8ff)
  case ChatMessageColor
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#dde6ef"></span>
  /// Alpha: 100% <br/> (0xdde6efff)
  case LightBlueProgress
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#263238"></span>
  /// Alpha: 100% <br/> (0x263238ff)
  case TopBarColor

  var rgbaValue: UInt32 {
    switch self {
    case .LoginButtonNormal: return 0x2ecc71ff
    case .LoginTextFiledColor: return 0xe9e7e5ff
    case .BlueProgress: return 0x1cb0f6ff
    case .ChatMessageColor: return 0xddf1d8ff
    case .LightBlueProgress: return 0xdde6efff
    case .TopBarColor: return 0x263238ff
    }
  }

  var color: Color {
    return Color(named: self)
  }
}
// swiftlint:enable type_body_length

extension Color {
  convenience init(named name: ColorName) {
    self.init(rgbaValue: name.rgbaValue)
  }
}

