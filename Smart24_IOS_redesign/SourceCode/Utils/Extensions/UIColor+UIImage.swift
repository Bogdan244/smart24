//
//  UIColor+UIImage.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/17/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

extension UIColor{
    func toImage() -> UIImage {
        let rect =  CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContextWithOptions(rect.size, true, 0)
        self.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
