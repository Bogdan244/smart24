//
//  AppDelegate+SIP.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/22/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
/*
extension AppDelegate {
    
    func configureEndpoint() {
        
        guard let udp = SWTransportConfiguration(transportType: SWTransportType.TCP) else {
            debugPrint("can't create SWTransportConfiguration")
            return
        }
        
        let endpointConfiguration = SWEndpointConfiguration(transportConfigurations: [udp])
        let endpoint = SWEndpoint.shared()
        
        endpoint?.configure(endpointConfiguration) { error in
            if let error = error {
                debugPrint("can't configure sip endpoint \(error)")
                
//                endpoint?.reset{ (error) in
//                    if let error = error {
//                        debugPrint(error)
//                    }
//                }
            }
        }
        
        endpoint?.setIncomingCall { (account, call) in
            debugPrint("Incoming Call : \(call?.callId)")
        }
        
        endpoint?.setAccountStateChange { (account) in
            
            guard let status = account?.accountState else {
                return
            }
            
            switch status {
            case .connected:
                debugPrint("accountState connected")
            case .connecting:
                debugPrint("accountState connecting")
            case .disconnected:
                debugPrint("accountState disconected")
            case .offline:
                debugPrint("accountState offline")
            }
        }
        
        endpoint?.setCallStateChange { (account, call) in
            debugPrint("Call State: \(call?.callState.rawValue)")
        }
        
        endpoint?.setCallMediaStateChange { (account, call) in
            debugPrint("Media State Changed")
        }
    }
    
    func addSipAccount() {
        if let accounts = SWEndpoint.shared().accounts as? [SWAccount] {
            debugPrint("accounts count when first run \(accounts.count)")
            
            accounts.forEach { account in
                account.endAllCalls()
                SWEndpoint.shared().removeAccount(account)
                SWEndpoint.shared().reset { error in
                    if let error = error {
                        debugPrint(error)
                    }
                }
            }
        }
        
        let account = SWAccount()
        
        let configuration = SWAccountConfiguration()
        configuration.username = Constants.sipUsername
        configuration.password = Constants.sipPassword
        configuration.domain = Constants.sipDomain
        configuration.address = SWAccountConfiguration.address(fromUsername: configuration.username, domain: configuration.domain)
        configuration.proxy = Constants.sipProxy
        configuration.registerOnAdd = false
        
        account.configure(configuration) { (error) in
            if let error = error {
                debugPrint(error)
            }
        }
    }
    
} */
