//
//  UIImage+SelectionIndicator.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/8/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import UIKit

extension UIImage {
    
    func createSelectionIndicator(color: UIColor, size: CGSize, lineWidth: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(CGRect(origin: CGPoint(x: 0,y: 0), size: CGSize(width: size.width, height: lineWidth)))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
