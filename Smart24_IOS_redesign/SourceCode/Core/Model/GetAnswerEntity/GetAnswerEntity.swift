//
//  GetAnswerEntity.swift
//  Smart24_IOS_redesign
//
//  Created by Vitya on 12/2/16.
//  Copyright © 2016 Vitya. All rights reserved.
//

import Foundation
import ObjectMapper

class GetAnswerEntity: BaseEntity {
    
    dynamic var expert_id = 0
    dynamic var messages: [MessagesEntity]?
    dynamic var cc_id = ""
    
    required convenience init?(_ map: Map) {
        self.init()
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        messages         <- map[MapperKey.messages]
        expert_id        <- map[MapperKey.expert_id]
        cc_id            <- map[MapperKey.cc_id]
    }
    
    override class func objectForMapping(map: Map) -> BaseMappable? {
        return GetAnswerEntity()
    }
    
    override static func ignoredProperties() -> [String] {
        return ["messages"]
    }
    
}
