//
//  ApiManager.swift
//  ULC
//
//  Created by Alex on 6/4/16.
//  Copyright © 2016 wezom.com.ua. All rights reserved.
//

import Foundation
import Moya

// MARK: - Provider setup
private func JSONResponseDataFormatter(data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data as Data, options: [])
        let prettyData = try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData as Data
    } catch {
        return data // fallback to original data if it cant be serialized
    }
}

// MARK: - Provider support
private extension String {
    var URLEscapedString: String {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed)!
    }
}

public enum ApiManager {
    case login(String, String)
    case registration(String, String, String, String, String, String)
    case confirmRegistration(String, String)
    case forgotPassword(String, String?)
    case loginWithSocial(Int, String)
    case registerWithSocial(String, String, Int, String)
    case news(String, Int)
    case notifications(String)
    case syncNotifications(Int, Int, String)
    case activateService(String, String)
    case getActiveServices(String, Int)
    case changeUserData(String, String?, String, String, String, String, String)
    case getUserInfo(String)
    case putInfoBackup(String, Bool, Int)
    case getInfoBackup(String)
    case sendMessage(String, String, String?, Int?)
    case getAnswer(String, String, Int?, Int?)
    case messagesRead(String, String, Int)
    case closeDialog(String, String, String, String?)
}

extension ApiManager: TargetType {
    
    public var baseURL: URL {return URL(string: Constants.BASE_URL)!}
    
    public var method: Moya.Method {
        switch self {
            
        case .login, .registration, .loginWithSocial, .changeUserData, .getUserInfo, .sendMessage, .syncNotifications, .confirmRegistration, .forgotPassword:
            return .post
            
        default :
            return .get
        }
    }
    
    public var path: String {
        
        switch self {
        case .login(_, _):
            return "login"
        case .registration(_, _, _, _, _, _):
            return "register"
        case .confirmRegistration(_, _):
            return "checkcode"
        case .forgotPassword:
            return "sendpasswd"
        case .loginWithSocial(_, _):
            return "loginSocial"
        case .registerWithSocial(_, _, _, _):
            return "registerSocial"
        case .news(_):
            return "news"
        case .notifications(_):
            return "notifications"
        case .syncNotifications(_, _, _):
            return "sync-notifications"
        case .activateService(_, _):
            return "activatecard"
        case .getActiveServices(_, _):
            return "services"
        case .changeUserData(_, _, _, _, _, _, _):
            return "changedata"
        case .getUserInfo(_):
            return "user_info"
        case .putInfoBackup(_):
            return "putinfo_backup"
        case .getInfoBackup(_):
            return "getinfo_backup"
        case .sendMessage(_, _, _, _):
            return "sendmessage"
        case .getAnswer(_, _, _, _):
            return "getanswer"
        case .messagesRead(_, _, _):
            return "messagesread"
        case .closeDialog(_, _, _, _):
            return "closedialog"
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var parameters: [String : Any]? {
        var value = [String: Any]()
        
        switch self {
            
        case .login(let email, let password):
            value = [ApiManagerKey.phone: email,
                     ApiManagerKey.passwd: password]
            
        case .registration(let name, let surname, let phone, let password, let confPass, let phoneModel):
            value = [ApiManagerKey.name: name,
                     ApiManagerKey.surname: surname,
                     ApiManagerKey.phone: phone,
                     ApiManagerKey.password: password,
                     ApiManagerKey.password_confirm: confPass,
                     ApiManagerKey.phone_model: phoneModel]
            
            
        case .confirmRegistration(let phone, let code):
            value = [ApiManagerKey.phone: phone,
                     ApiManagerKey.code: code]
            
        case .forgotPassword(let phone, let code):
            if let code = code {
                value = [ApiManagerKey.phone: phone,
                         ApiManagerKey.code: code]
            } else {
                value = [ApiManagerKey.phone: phone]
            }
            
        case .loginWithSocial(let type, let id):
            value = [ApiManagerKey.social_type: type,
                     ApiManagerKey.social_id: id]
            
        case .registerWithSocial(let photo, let username, let type, let id):
            value = [ApiManagerKey.user_photo: photo,
                     ApiManagerKey.username: username,
                     ApiManagerKey.social_type: type,
                     ApiManagerKey.social_id: id]
            
        case .news(let id, let density):
            value = [ApiManagerKey.uid: id,
                     ApiManagerKey.density: density]
            
        case .notifications(let id):
            value = [ApiManagerKey.uid: id]
            
        case .syncNotifications(let id, let notificationsId, let action):
            value = [ApiManagerKey.uid: id,
                     ApiManagerKey.firstNotification: [ApiManagerKey.id: notificationsId,
                                                       ApiManagerKey.action: action]
            ]
            
        case .activateService(let userId, let activationCode):
            value = [ApiManagerKey.uid: userId,
                     ApiManagerKey.activ_code: activationCode]
            
        case .getActiveServices(let userId, let density):
            value = [ApiManagerKey.uid: userId,
                     ApiManagerKey.density: density]
            
        case .changeUserData(let userId, let userPhoto, let userName, let surname, let phone, let password, let confirmPassword):
            
            if let userPhoto = userPhoto {
                value = [ApiManagerKey.uid: userId,
                         ApiManagerKey.image_person: "data:image/png;base64," + userPhoto,
                         ApiManagerKey.name: userName,
                         ApiManagerKey.surname: surname,
                         ApiManagerKey.phone: phone,
                         ApiManagerKey.passwd: password,
                         ApiManagerKey.passwdcheck: confirmPassword]
            } else {
                value = [ApiManagerKey.uid: userId,
                         ApiManagerKey.name: userName,
                         ApiManagerKey.surname: surname,
                         ApiManagerKey.phone: phone,
                         ApiManagerKey.passwd: password,
                         ApiManagerKey.passwdcheck: confirmPassword]
            }
            
        case .getUserInfo(let userId):
            value = [ApiManagerKey.uid: userId]
            
        case .putInfoBackup(let userId, let isBackup, let date):
            value = [ApiManagerKey.uid: userId,
                     ApiManagerKey.iSbackup: isBackup,
                     ApiManagerKey.date: date]
            
        case .getInfoBackup(let userId):
            value = [ApiManagerKey.uid: userId]
            
        case .sendMessage(let userId, let message, let dialog_id, let isVideo):
            if let dialog_id = dialog_id, let isVideo = isVideo {
                value = [ApiManagerKey.uid: userId,
                         ApiManagerKey.message: message,
                         ApiManagerKey.dialog_id: dialog_id,
                         ApiManagerKey.isVideo: isVideo]
            } else if let dialog_id = dialog_id {
                value = [ApiManagerKey.uid: userId,
                         ApiManagerKey.message: message,
                         ApiManagerKey.dialog_id: dialog_id]
            } else if let isVideo = isVideo {
                value = [ApiManagerKey.uid: userId,
                         ApiManagerKey.message: message,
                         ApiManagerKey.isVideo: isVideo]
            } else {
                value = [ApiManagerKey.uid: userId,
                         ApiManagerKey.message: message]
            }
            
        case .getAnswer(let userId, let dialog_id, let timestamp, let isVideo):
            if let timestamp = timestamp, let isVideo = isVideo {
                value = [ApiManagerKey.uid: userId,
                         ApiManagerKey.dialog_id: dialog_id,
                         ApiManagerKey.timestamp: timestamp,
                         ApiManagerKey.isVideo: isVideo]
            } else if let timestamp = timestamp {
                value = [ApiManagerKey.uid: userId,
                         ApiManagerKey.dialog_id: dialog_id,
                         ApiManagerKey.timestamp: timestamp]
            } else if let isVideo = isVideo {
                value = [ApiManagerKey.uid: userId,
                         ApiManagerKey.dialog_id: dialog_id,
                         ApiManagerKey.isVideo: isVideo]
            } else {
                value = [ApiManagerKey.uid: userId,
                         ApiManagerKey.dialog_id: dialog_id]
            }
            
        case .messagesRead(let userId, let dialog_id, let timestamp):
            value = [ApiManagerKey.uid: userId,
                     ApiManagerKey.dialog_id: dialog_id,
                     ApiManagerKey.timestamp: timestamp]
            
        case .closeDialog(let userId, let dialog_id, let grade, let opinion):
            if let opinion = opinion {
                value = [ApiManagerKey.uid: userId,
                         ApiManagerKey.dialog_id: dialog_id,
                         ApiManagerKey.grade: grade,
                         ApiManagerKey.opinion: opinion]
            } else {
                value = [ApiManagerKey.uid: userId,
                         ApiManagerKey.dialog_id: dialog_id,
                         ApiManagerKey.grade: grade]
            }
        }
        
        return value
        
    }
    
    public var task: Moya.Task {
        return Moya.Task.request
    }
}

let plugins: [PluginType] = [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)]

let networkProvider = MoyaProvider<ApiManager>(endpointClosure: endpointClosure, plugins: plugins)

