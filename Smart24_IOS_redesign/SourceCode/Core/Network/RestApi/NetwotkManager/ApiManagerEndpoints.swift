//
//  ApiManagerEndpoints.swift
//  ULC
//
//  Created by Alex on 6/4/16.
//  Copyright © 2016 wezom.com.ua. All rights reserved.
//

import Foundation
import Moya
import SwiftKeychainWrapper

let endpointClosure = {(target: ApiManager) -> Endpoint<ApiManager> in

    var httpFields = [String: String]()
    httpFields["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";
    
    let endpoint: Endpoint<ApiManager> = Endpoint<ApiManager>(url: target.baseURL.appendingPathComponent(target.path).absoluteString,
                                                              sampleResponseClosure: {.networkResponse(200, target.sampleData)},
                                                              method: target.method,
                                                              parameters: target.parameters,
                                                              httpHeaderFields: httpFields)
    
    switch target.method {
    case .post, .put:
        return endpoint.adding(newParameterEncoding: URLEncoding.default)
    default:
        return endpoint
    }
}

enum ApiManagerKey {
    
    static let email            = "email"
    static let token            = "token"
    static let login            = "login"
    static let passwd           = "passwd"
    static let username         = "username"
    static let name             = "name"
    static let surname          = "surname"
    static let phone            = "phone"
    static let social_type      = "social_type"
    static let social_id        = "social_id"
    static let user_photo       = "user_photo"
    static let uid              = "uid"
    static let activ_code       = "activation_code"
    static let password         = "password"
    static let passwdcheck      = "passwdcheck"
    static let image_person     = "image_person"
    static let iSbackup         = "isbackup"
    static let date             = "date"
    static let dialog_id        = "dialog_id"
    static let message          = "message"
    static let timestamp        = "timestamp"
    static let grade            = "grade"
    static let opinion          = "opinion"
    static let notifications    = "notifications"
    static let id               = "id"
    static let action           = "action"
    static let read             = "read"
    static let delete           = "delete"
    static let firstNotification = "notifications[0]"
    static let density          = "density"
    static let password_confirm = "password_confirm"
    static let phone_model      = "phone_model"
    static let code             = "code"
    static let isVideo          = "isVideo"
}
